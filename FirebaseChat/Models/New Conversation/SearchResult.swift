//
//  SearchResult.swift
//  FirebaseChat
//
//  Created by Raj Kadam on 25/05/21.
//

import Foundation

struct SearchResult {
    let name: String
    let email: String
    //let isGroup: Bool
    var isSelect: Bool?
}

protocol NewConversationDelegate: AnyObject {
    
    func createOneToOneConversationWith(user: SearchResult)
    func createGroupConversationWith(users: [SearchResult], groupName: String, groupImage: Data?)
}

struct GroupData {
    let groupId: String?
    let members: [SearchResult]
    let name: String
    let imageData: Data?
}
