//
//  ConversationListModel.swift
//  FirebaseChat
//
//  Created by Raj Kadam on 22/05/21.
//

import Foundation

struct Conversation {
    let id: String
    let name: String // group name
    let sender_name: String // msg sender name
    let other_user_email: String? // sender email
    let latest_message: LatestMessage
    let channel_type: String
    let members: [GroupMembers]?
}


struct GroupMembers {
    let name: String
    let email: String
    let isAdmin: Bool
}
struct LatestMessage {
    let messageDate: Date
    let date: String
    let message: String
    let is_read: Bool
    let type: String
}
