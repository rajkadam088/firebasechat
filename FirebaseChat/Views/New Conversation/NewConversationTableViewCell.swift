//
//  NewConversationTableViewCell.swift
//  FirebaseChat
//
//  Created by Raj Kadam on 25/05/21.
//

import UIKit

class NewConversationTableViewCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userSelectImageView: UIImageView!
    @IBOutlet weak var userName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialUISetup()
    }

    func initialUISetup(){
        userImageView.layer.cornerRadius = userImageView.frame.height/2
        userImageView.layer.masksToBounds = true
        userImageView.clipsToBounds = true
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configure(with model: SearchResult, isGroup: Bool) {
        
        if isGroup {
            userSelectImageView.isHidden = false
            if let isSelect = model.isSelect, isSelect {
                userSelectImageView.image = UIImage.init(systemName: "checkmark.square.fill")
                //
            }else{
                userSelectImageView.image = UIImage.init(systemName: "checkmark.square")
            }
        }else{
            userSelectImageView.isHidden = true
        }
        let path = "images/\(model.email)\(UserConstants.profilePicFileNameExtension)"
        StorageManager.shared.downloadURL(for: path) { [weak self] result in
            
            switch result {
            case .success(let url):
                DispatchQueue.main.async {
                    self?.userImageView.sd_setImage(with: url, completed: nil)
                }
                
                
            case .failure(let error):
                print("Failed to get url: \(error)")
            }
            
        }
        userName.text = model.name 
    }
    
    
}
