//
//  SelectedGroupMemberCollcetionViewCell.swift
//  FirebaseChat
//
//  Created by Raj Kadam on 01/06/21.
//

import UIKit

class SelectedGroupMemberCollcetionViewCell: UICollectionViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    public func configure(with model: SearchResult) {
        
        let path = "images/\(model.email)\(UserConstants.profilePicFileNameExtension)"
        StorageManager.shared.downloadURL(for: path) { [weak self] result in
            
            switch result {
            case .success(let url):
                DispatchQueue.main.async {
                    self?.userImageView.sd_setImage(with: url, completed: nil)
                }
                
                
            case .failure(let error):
                print("Failed to get url: \(error)")
            }
            
        }
    }
    
    
}
