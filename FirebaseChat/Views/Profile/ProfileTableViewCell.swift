//
//  ProfileTableViewCell.swift
//  FirebaseChat
//
//  Created by Raj Kadam on 26/05/21.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCellWith(viewModel: ProfileViewModel) {
        titleLabel.text = viewModel.title
        switch viewModel.viewModelType {
        case .info:
            break
        case .logout:
            titleLabel.textColor = .red
            
        }
    }
    
}
