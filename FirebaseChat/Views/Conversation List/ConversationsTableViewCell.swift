//
//  ConversationsTableViewCell.swift
//  FirebaseChat
//
//  Created by Raj Kadam on 22/05/21.
//

import UIKit
import SDWebImage

class ConversationsTableViewCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var imageMessageType: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var lastMessage: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupUI()
    }

    func setupUI(){
        userImageView.layer.cornerRadius = userImageView.bounds.width / 2
        userImageView.clipsToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configure(with model: Conversation) {
        var path = ""
        
        if model.channel_type == "group" {
            userImageView.image = UIImage.init(systemName: "person.and.person.fill")
            var id = model.id
//            id  = id.replacingOccurrences(of: " ", with: "")
//            id = id.replacingOccurrences(of: ":", with: "")
//            id = id.replacingOccurrences(of: ",", with: "")
//            id = id.replacingOccurrences(of: "+", with: "")
            id = "\(id).png"
            path = "images/\(id)"
            
        }else {
            userImageView.image = UIImage.init(systemName: "person.fill")
            let email = model.other_user_email ?? ""
            path = "images/\(email)\(UserConstants.profilePicFileNameExtension)"
        }
       
        StorageManager.shared.downloadURL(for: path) { [weak self] result in
            
            switch result {
            case .success(let url):
                DispatchQueue.main.async {
                    self?.userImageView.sd_setImage(with: url, completed: nil)
                }
                
                
            case .failure(let error):
                print("Failed to get url: \(error)")
                
            }
            
        }
        
        if model.latest_message.type == "photo" {
            imageMessageType.isHidden = false
            imageMessageType.image = UIImage.init(systemName: "photo")
            lastMessage.text = "Photo"
            imageMessageType.tintColor = .darkGray
        }else if model.latest_message.type == "video" {
            imageMessageType.isHidden = false
            imageMessageType.image = UIImage.init(systemName: "video")
            imageMessageType.tintColor = .darkGray
            lastMessage.text = "Video"
        }else if model.latest_message.type == "location" {
            imageMessageType.isHidden = false
            imageMessageType.image = UIImage.init(systemName: "location.fill")
            imageMessageType.tintColor = .darkGray
            lastMessage.text = "Location"
        }
        else {
            // type == text
            imageMessageType.isHidden = true
            lastMessage.text = model.latest_message.message
        }
        userName.text = model.name
        
        print("Message UTC Date: \(model.latest_message.date)")
        print("Message Local Date: \(model.latest_message.date.toLocalDateString())")
    }
    
}
