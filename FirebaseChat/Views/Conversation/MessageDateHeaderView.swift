//
//  MessageDateHeaderView.swift
//  FirebaseChat
//
//  Created by Raj Kadam on 29/05/21.
//

import UIKit
import MessageKit

class MessageDateHeaderView: MessageReusableView {

     var dateLabel: UILabel = {
       let label = UILabel()
        label.backgroundColor = .clear
        label.textAlignment = .center
        return label
    }()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.addSubview(dateLabel)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
   
    func addDate(){
        dateLabel.frame = CGRect.init(x: 0, y: 0, width: self.width, height: 40)
        addSubview(dateLabel)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
       // dateLabel.frame = CGRect.init(x: 0, y: 0, width: self.width, height: 40)
    
    }
    
}
