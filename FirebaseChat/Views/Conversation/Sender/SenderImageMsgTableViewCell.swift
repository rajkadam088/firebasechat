//
//  SenderImageMsgTableViewCell.swift
//  FirebaseChat
//
//  Created by Raj Kadam on 17/06/21.
//

import UIKit

class SenderImageMsgTableViewCell: UITableViewCell {
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var messageContainerView: UIView!
    @IBOutlet weak var contentImage: UIImageView!
    //@IBOutlet weak var btnImage: RKButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func uiInitilization(){
        self.messageContainerView.layer.cornerRadius = 5
        self.contentImage.layer.cornerRadius = 3
        self.messageContainerView.clipsToBounds = true
        self.messageContainerView.layoutIfNeeded()
        
        setupShadow()
        
    }
    
    func setupShadow(){
        self.messageContainerView.layer.shadowColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.2).cgColor
        self.messageContainerView.layer.shadowOffset = CGSize.zero//init(width: 10, height: 10)
        self.messageContainerView.layer.shadowRadius = 1
        self.messageContainerView.layer.shadowOpacity = 0.8
        self.messageContainerView.layer.masksToBounds = false
        
    }
    
    func setupCell(){
        uiInitilization()
        
    }
    
    func setupCell(message: Message){
        
        switch message.kind {
        case .photo(let media):
            contentImage.sd_setImage(with: media.url, completed: nil)
        default:
            break
        }
       
        
    }
    
    /*func setupCell(data: ConversationData){
        
        if let imageString = data.content?.content_str,let imageUrl = URL.init(string: imageString)
        {
            contentImage.sd_setImage(with: imageUrl, placeholderImage: Constants.placeholderImage)
            btnImage.additinalText = imageString
        } else {
            contentImage.image = Constants.placeholderImage
        }
        
        guard let date = data.date else {return}
        let dateFormatter = DateFormatter()
          dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
          dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        
          
          let headerDate = dateFormatter.date(from: date) ?? Date()
          
          dateFormatter.dateStyle = .none
              //ss.SSSZZZZZ
              dateFormatter.timeStyle = .short
          dateFormatter.timeZone =  .current//TimeZone.init(abbreviation: "IST")
        let convertedString = dateFormatter.string(from: headerDate)
        
        timeLabel.text = "\(convertedString)"
        
    }*/
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
