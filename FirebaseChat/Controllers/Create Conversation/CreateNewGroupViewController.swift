//
//  CreateNewGroupViewController.swift
//  FirebaseChat
//
//  Created by Raj Kadam on 01/06/21.
//

import UIKit

class CreateNewGroupViewController: UIViewController {

    public var groupCompletion: ((GroupData) -> (Void))?
    
    @IBOutlet weak var groupImage: UIImageView!
    @IBOutlet weak var groupName: UITextField!
    @IBOutlet weak var participantsLabel: UILabel!
    @IBOutlet weak var groupMembersCollection: UICollectionView!
    
    var groupMembers: [SearchResult]?
    var selectedImageData: Data?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        groupImage.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(imageTapGesture))
        
        groupImage.addGestureRecognizer(tap)
        registerNibs()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let members = groupMembers {
            participantsLabel.text = "Participants: \(members.count)"
        }
    }

    func registerNibs(){
        
        groupMembersCollection.register(UINib.init(nibName: "SelectedGroupMemberCollcetionViewCell", bundle: nil), forCellWithReuseIdentifier: "SelectedGroupMemberCollcetionViewCell")
    }
    
    @objc private func imageTapGesture(){
        
        presentPhotoActionSheet()
    }
    
    @IBAction func didTapCancel(){
//        self.navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTapCreate(){
//        self.navigationController?.popViewController(animated: true)
        guard let name = groupName.text , name.count > 3 else{
            print("Enter valid group name")
            return
        }
        guard let members = groupMembers else {
            print("Add members")
            return
        }
        dismiss(animated: true) {  [weak self] in
            let group = GroupData.init(groupId: nil, members: members, name: name, imageData: self?.selectedImageData)
            self?.groupCompletion?(group)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CreateNewGroupViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    
    {
        return CGSize.init(width: 50, height: 50)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return groupMembers?.count ?? 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectedGroupMemberCollcetionViewCell", for: indexPath) as! SelectedGroupMemberCollcetionViewCell
        
        
        return cell
        
    }
    
    
}
extension CreateNewGroupViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func presentPhotoActionSheet(){
        
        let actionSheet = UIAlertController.init(title: "Group Icon", message: "How would you like to select a picture", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction.init(title: "Take Photo", style: .default, handler: { [weak self] _ in
            self?.presentCamera()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Choose Photo", style: .default, handler: { [weak self] _ in
            self?.presentPhotoPicker()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        present(actionSheet, animated: true, completion: nil)
    }
    
    func presentCamera(){
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.delegate = self
        vc.allowsEditing = true
        present(vc, animated: true, completion: nil)
    }
    
    func presentPhotoPicker(){
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        vc.delegate = self
        vc.allowsEditing = true
        present(vc, animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let image = info[.editedImage] as? UIImage else {
            picker.dismiss(animated: true, completion: nil)
            return
        }
        groupImage.image = image
        self.selectedImageData = image.pngData()
        picker.dismiss(animated: true, completion: nil)
    }
    
    
}
