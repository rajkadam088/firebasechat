//
//  NewconversationViewController.swift
//  FirebaseChat
//
//  Created by Raj Kadam on 20/05/21.
//

import UIKit
import JGProgressHUD

class NewconversationViewController: UIViewController {
    
    public var completion: ((SearchResult) -> (Void))?
    public var groupCompletion: ((GroupData) -> (Void))?
    //public var groupSelectCompletion: ((GroupData) -> (Void))?
    public var delegate: NewConversationDelegate?
    private let spinner = JGProgressHUD.init(style: .dark)
    
    private var users = [[String: Any]]()
    private var results = [SearchResult]()
    private var hasFetched = false
    private let noResultLabel: UILabel = {
       let label = UILabel()
        label.text = "No Results"
        label.textAlignment = .center
        label.textColor = .gray
        label.font = UIFont.systemFont(ofSize: 21, weight: .medium)
        label.isHidden = true
        return label
    }()
    
    public var selfSender: Sender? {
        guard let email = SharedPref.shared.getemail() else {
            return nil
        }
        let safeEmail = DatabaseManager.safeEmail(email: email)
        let name = SharedPref.shared.getFullName() ?? "User"
        return Sender.init(photoURL: "", senderId: safeEmail, displayName: name)
    }
    
    @IBOutlet weak var searachBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    var isGroup = false
    var groupMembers: [SearchResult]?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "New Conversation"
        view.addSubview(noResultLabel)
        tableView.register(UINib.init(nibName: "NewConversationTableViewCell", bundle: nil), forCellReuseIdentifier: "NewConversationTableViewCell")
        tableView.allowsMultipleSelection = false
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
       noResultLabel.frame = CGRect.init(x: 20, y: (tableView.height/2)-20, width: tableView.width-40, height: 40)
    }
    func presentationControllerShouldDismiss(_ presentationController: UIPresentationController) -> Bool {
        return false
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //navigationController?.navigationBar.topItem?.titleView = searachBar
        searachBar.becomeFirstResponder()
    }
    @IBAction func newGroup(){
        
        isGroup = true
        tableView.allowsMultipleSelection = true
        tableView.reloadData()
    }
    @IBAction func didTapCancel(){
//        self.navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTapCreate(){
//        self.navigationController?.popViewController(animated: true)
        //dismiss(animated: true, completion: nil)
        let storyboard = UIStoryboard.init(name: "Chat", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "CreateNewGroupViewController") as! CreateNewGroupViewController
       
        vc.groupCompletion = { [weak self] groupData in
            if let view = self?.view {
            self?.spinner.show(in: view)
                self?.spinner.textLabel.text = "Creating group"
            }
            self?.createNewGroup(groupData: groupData)
            
            
        }
        vc.groupMembers = groupMembers
        let nav = UINavigationController.init(rootViewController: vc)
        nav.isModalInPresentation = true
        nav.navigationItem.largeTitleDisplayMode = .never
        nav.modalPresentationStyle = .popover
        present(nav, animated: true, completion: nil)
    }
    
    func createNewGroup(groupData: GroupData){
        guard let selfSender = selfSender, let messageID = createMessageID(name: groupData.name), let email = SharedPref.shared.getemail() else {
            print("group members are empty")
            spinner.dismiss()
            return
        }
        let name = SharedPref.shared.getFullName() ?? "nFormation Admin"
        let text = "\(name) has created group."
        let safeEmail = DatabaseManager.safeEmail(email: email)
        let message = Message.init(sender: selfSender, messageId: messageID,  sentDate: Date(), kind: .text(text), senderName: name, senderEmail: safeEmail, isRead: true)
        
        DatabaseManager.shared.createNewGroupConversation(with: groupData.members, groupName: groupData.name, firstMessage: message) { [weak self] created in
            
            if created {
                print("Group Created and message Sent")
                let groupid = "group_\(messageID)"
                let newGroupData = GroupData.init(groupId: groupid, members: groupData.members, name: groupData.name, imageData: groupData.imageData)
                
                self?.uploadGroupImage(groupData: newGroupData)
            }else{
                print("Failed to Create group")
                self?.spinner.dismiss()
            }
        }
    }
    
    func uploadGroupImage(groupData: GroupData) {
        spinner.textLabel.text = "Uploading group image"
        // upload image
        guard let imageData = groupData.imageData, let id = groupData.groupId else {
            spinner.dismiss()
            dismiss(animated: true, completion: { [weak self] in
                self?.groupCompletion?(groupData)
            })
            return
        }
        let fileName = "\(id).png"
        StorageManager.shared.uploadProfilePicture(with: imageData, fileName: fileName) { [weak self] result in
            self?.spinner.dismiss()
            switch result {
            case .success(let downloadUrl):
                
                print("group image download url: \(downloadUrl)")
                self?.dismiss(animated: true, completion: { [weak self] in
                    self?.groupCompletion?(groupData)
                })
                
            case .failure(_):
                
                break
            }
        }
        
    }
    
    func createMessageID(name: String) -> String? {
        // date, otherUserEmail, SenderEmail, randomInt
        let dateString = ChatViewController.dateFormatter.string(from: Date())
        guard let currentUserEmail = SharedPref.shared.getemail()  else {
            return nil
        }
        let safeEmail = DatabaseManager.safeEmail(email: currentUserEmail)
        let newIdentifier = "\(name)_\(safeEmail)_\(dateString)"
        
        print("Created message id: \(newIdentifier)")
        return newIdentifier
    }
    
}

extension NewconversationViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searachBar.resignFirstResponder()
        
        guard let text = searchBar.text, !text.replacingOccurrences(of: " ", with: "").isEmpty else {
            return
        }
        results.removeAll()
        spinner.show(in: view)
        searchUser(query: text)
        
    }
    
    func searchUser(query: String) {
        // check if aaray has firebase result
        if hasFetched {
            // if it does: filter
            filterUsers(with: query)
        }else{
            // if not, fetch from Server and then Filter
            DatabaseManager.shared.getAllUsers (completion: { [weak self] result in
                
                switch result {
                case .success(let collection):
                    self?.hasFetched = true
                self?.users = collection
                    self?.filterUsers(with: query)
                case .failure(let error):
                    print("Failed to get user: \(error)")
                    self?.spinner.dismiss()
                }
            })
        }
    
    }
    
    func filterUsers(with term: String){
        spinner.dismiss()
        guard let currentUserEmail = SharedPref.shared.getemail(), hasFetched else {
            return
        }
        let safeEmail = DatabaseManager.safeEmail(email: currentUserEmail)
        
        // Update UI
        let updatedResults: [SearchResult] = users.filter ({
            guard let email = $0[UserDatakey.email] as? String, email != safeEmail else {
                return false
            }
            guard var name = $0[UserDatakey.name] as? String else {
                return false
            }
            name = name.lowercased()
            return name.contains(term.lowercased())
            //return name.hasPrefix(term.lowercased())
        }).compactMap ({
            guard let email = $0[UserDatakey.email] as? String, let name = $0[UserDatakey.name] as? String else {
                return nil
            }
            
            return SearchResult.init(name: name, email: email, isSelect: false)
        })
        
        results = updatedResults
        
        updateUI()
    }
    
    func updateUI(){
        
        if results.isEmpty {
            noResultLabel.isHidden = false
            tableView.isHidden = true
        }else {
            noResultLabel.isHidden = true
            tableView.isHidden = false
            tableView.reloadData()
        }
    }
}


extension NewconversationViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewConversationTableViewCell", for: indexPath) as! NewConversationTableViewCell
        
        cell.configure(with: results[indexPath.row], isGroup: isGroup)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isGroup {
            if groupMembers == nil {
                groupMembers = [SearchResult]()
            }
            var targetUserData = results[indexPath.row]
            if let index = groupMembers?.firstIndex(where: {
                guard let userEmail = $0.email as? String else{
                    return false
                }
                return targetUserData.email == userEmail
            }){
                targetUserData.isSelect = false
                results[indexPath.row] = targetUserData
                tableView.reloadRows(at: [indexPath], with: .automatic)
                groupMembers?.remove(at: index)
                return
            }else{
                targetUserData.isSelect = true
                results[indexPath.row] = targetUserData
                tableView.reloadRows(at: [indexPath], with: .automatic)
                groupMembers?.append(targetUserData)
                return
            }
            
        }
        tableView.deselectRow(at: indexPath, animated: true)
        // start conversation
        let targetUserData = results[indexPath.row]
        
        //navigationController?.popViewController(animated: false)
        //delegate?.createOneToOneConversationWith(user: targetUserData)
        dismiss(animated: true, completion: { [weak self] in
            self?.completion?(targetUserData)
        })
    }
    
    
}
