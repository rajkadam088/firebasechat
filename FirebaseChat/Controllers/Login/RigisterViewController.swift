//
//  RigisterViewController.swift
//  FirebaseChat
//
//  Created by Raj Kadam on 20/05/21.
//

import UIKit
import FirebaseAuth
import JGProgressHUD
class RigisterViewController: UIViewController {
    private let spinner = JGProgressHUD.init(style: .dark)
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(imageTapGesture))
        
        profileImage.addGestureRecognizer(tap)
        initialUISetup()
    }
    
    func initialUISetup() {
        profileImage.layer.masksToBounds = true
        profileImage.layer.cornerRadius = profileImage.width/2.0
        
        
    }
    
    static func loadFromNib() -> RigisterViewController{
        let storyboard = UIStoryboard.init(name: "Register", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "RigisterViewController") as! RigisterViewController
        return vc
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @objc private func imageTapGesture(){
        
        presentPhotoActionSheet()
    }
    @IBAction func registerAction(){
        
        emailText.resignFirstResponder()
        passwordText.resignFirstResponder()
        guard let firstName = firstName.text, let lastName = lastName.text, let email = emailText.text, let password = passwordText.text, !lastName.isEmpty, !firstName.isEmpty, !email.isEmpty, !password.isEmpty else{
            alertUserLoginError()
            return
            
        }
        spinner.show(in: view)
        // check user Is available
        DatabaseManager.shared.userExists(with: email) { [weak self] isExist in
            
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                strongSelf.spinner.dismiss()
            }
            if isExist {
                strongSelf.alertUserLoginError(message: "Email Id is already exists")
            }else{
                //strongSelf.registerUser(user: ChatAppUser.init(firstName: firstName, lastName: lastName, emailAddress: email), password: password)
                FirebaseAuth.Auth.auth().createUser(withEmail: email, password: password) { [weak self]
                    authResult, error in
                    guard let strongSelf = self else {return}
                    
                    guard authResult != nil, error == nil else{
                        return
                    }
                    SharedPref.shared.setemail(email: email)
                    SharedPref.shared.setfirstName(name: firstName)
                    SharedPref.shared.setlastName(name: lastName)
                    let chatUser = ChatAppUser.init(firstName: firstName, lastName: lastName, emailAddress: email)
                    
                    DatabaseManager.shared.insertUser(with: chatUser) { success in
                        
                        if success {
                            // upload image
                            guard let image =  strongSelf.profileImage.image, let imageData = image.pngData() else {
                                
                                return
                            }
                            let fileName = chatUser.profilePictureFileName
                            StorageManager.shared.uploadProfilePicture(with: imageData, fileName: fileName) { result in
                                
                                switch result {
                                case .success(let downloadUrl):
                                    
                                    SharedPref.shared.setProfileImage(url: URL.init(string: downloadUrl))
                                    print("profile image download url: \(downloadUrl)")
                                    
                                case .failure(_):
                                        
                                    break
                                }
                            }
                        }
                    }
                    Router.gotoConversation()
                    //strongSelf.navigationController?.dismiss(animated: true, completion: nil)
                }
            }
        }
        
        
    }
    
    func registerUser(user: ChatAppUser, password: String){
        // Firebase Login
        
        
        FirebaseAuth.Auth.auth().createUser(withEmail: user.emailAddress, password: password) { [weak self]
            authResult, error in
            guard let strongSelf = self else {return}
            
            guard authResult != nil, error == nil else{
                return
            }
            
            //DatabaseManager.shared.insertUser(with: user, completion: (Bool) -> Void)
            strongSelf.navigationController?.dismiss(animated: true, completion: nil)
        }
    }
    func alertUserLoginError(message: String = "Please enter all info to register!"){
        let alert = UIAlertController.init(title: "Woops", message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction.init(title: "Dismiss", style: .cancel, handler: { action in
            
        }))
        present(alert, animated: true, completion: nil)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension RigisterViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == firstName {
            lastName.becomeFirstResponder()
        }
        
        if textField == lastName {
            emailText.becomeFirstResponder()
        }
        if textField == emailText {
            passwordText.becomeFirstResponder()
        }
        if textField == passwordText {
            passwordText.resignFirstResponder()
            registerAction()
        }
        return true
        
    }
    
}

extension RigisterViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func presentPhotoActionSheet(){
        
        let actionSheet = UIAlertController.init(title: "Profile Picture", message: "How would you like to select a picture", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction.init(title: "Take Photo", style: .default, handler: { [weak self] _ in
            self?.presentCamera()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Choose Photo", style: .default, handler: { [weak self] _ in
            self?.presentPhotoPicker()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        present(actionSheet, animated: true, completion: nil)
    }
    
    func presentCamera(){
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.delegate = self
        vc.allowsEditing = true
        present(vc, animated: true, completion: nil)
    }
    
    func presentPhotoPicker(){
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        vc.delegate = self
        vc.allowsEditing = true
        present(vc, animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let image = info[.editedImage] as? UIImage else {
            picker.dismiss(animated: true, completion: nil)
            return
        }
        profileImage.image = image
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    
}
