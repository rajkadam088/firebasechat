//
//  LoginViewController.swift
//  FirebaseChat
//
//  Created by Raj Kadam on 20/05/21.
//

import UIKit
import FirebaseAuth
import JGProgressHUD
class LoginViewController: UIViewController {

    private let spinner = JGProgressHUD.init(style: .dark)
    private let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage.init(named: "logo")
        imageView.contentMode = .scaleAspectFill
       return imageView
    }()
    
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        title = "Login"
        // Do any additional setup after loading the view.
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Register", style: .done, target: self, action: #selector(didTapRegister))
        
        
        //view.addSubview(imageView)
    }
    
    static func loadFromNib() -> LoginViewController{
        let storyboard = UIStoryboard.init(name: "Register", bundle: nil)
        let loginVc = storyboard.instantiateViewController(identifier: "LoginViewController") as! LoginViewController
        return loginVc
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let size = view.width/3
        imageView.frame = CGRect.init(x: (view.width-size)/2, y: 20, width: size, height: size)
        emailText.leftView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 3, height: emailText.height))
    }

    @objc private func didTapRegister() {
        let vc = RigisterViewController.loadFromNib()
        vc.title = "Register"
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func loginAction(_ sender: UIButton){
        
        emailText.resignFirstResponder()
        passwordText.resignFirstResponder()
        guard let email = emailText.text, let password = passwordText.text, !email.isEmpty, !password.isEmpty else{
            alertUserLoginError()
            return
            
        }
        spinner.show(in: view)
        // Firebase Login
        
        FirebaseAuth.Auth.auth().signIn(withEmail: email, password: password) { [weak self] authResult, error in
            
            guard let strongSelf = self else {return}
            DispatchQueue.main.async {
                strongSelf.spinner.dismiss()
            }
            
            guard let result = authResult, error == nil else{
                print("Failed to login  ")
                return
            }
            let firebaseID = result.user.uid ?? ""
            let user = result.user.displayName ?? ""
            SharedPref.shared.setemail(email: email)
            let safeEmail = DatabaseManager.safeEmail(email: email)
            DatabaseManager.shared.getDataFor(path: "\(FolderName.Users)/\(safeEmail)") { result in
                
                switch result {
                    
                case .success(let data):
                    guard let userData = data as? [String: Any], let firstName = userData[UserDatakey.firstName] as? String, let lastName = userData[UserDatakey.lastName] as? String else {
                        return
                    }
                    SharedPref.shared.setfirstName(name: firstName)
                    SharedPref.shared.setlastName(name: lastName)
                    Router.gotoConversation()
                    print("Userdata: \(data)")
                case .failure( let error):
                    print("Failed to get data : \(error)")
                    
                }
            }
            //SharedPref.setlastName(user)
            print("Logged in User: \(user)")
            
            //strongSelf.navigationController?.dismiss(animated: true, completion: nil)
           
        }
    }
    
    func alertUserLoginError(){
        let alert = UIAlertController.init(title: "Woops", message: "Please enter all info to login!", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction.init(title: "Dismiss", style: .cancel, handler: { action in
            
        }))
        present(alert, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LoginViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailText {
            passwordText.becomeFirstResponder()
        }
        return true
        
    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        
//        
//    }
}
