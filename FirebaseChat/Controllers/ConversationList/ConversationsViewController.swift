//
//  ViewController.swift
//  FirebaseChat
//
//  Created by Raj Kadam on 20/05/21.
//

import UIKit
import FirebaseAuth
import JGProgressHUD


class ConversationsViewController: UIViewController {

    private var conversations = [Conversation]()
    private let spinner = JGProgressHUD.init(style: .dark)
    
    @IBOutlet weak var tableView: UITableView!

    private let noConversationLabel: UILabel = {
       let label = UILabel()
        label.text = "No conversations"
        label.textAlignment = .center
        label.textColor = .gray
        label.font = UIFont.systemFont(ofSize: 21, weight: .medium)
        label.isHidden = true
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .compose, target: self, action: #selector(didTapComposeButton))
        
        view.addSubview(noConversationLabel )
        setupTableView()
        startListinigForConverstions()
        
        dateSort()
        newDateSort()
    }
    
    func presentationControllerShouldDismiss(_ presentationController: UIPresentationController) -> Bool {
        return false
    }

    private func startListinigForConverstions() {
        guard let userEmail = SharedPref.shared.getemail() else {
            return
        }
        let safeEmail = DatabaseManager.safeEmail(email: userEmail)
        DatabaseManager.shared.getAllConversation(for: safeEmail) { [weak self] result in
            switch result {
            
            case .success(let conversations):
                guard !conversations.isEmpty, let strongSelf = self else {
                    return
                }
                strongSelf.conversations = conversations
                DispatchQueue.main.async {
                    
                    strongSelf.tableView.reloadData()
                    //strongSelf.tableView.beginUpdates()
                    //strongSelf.tableView.endUpdates()
                }
               
            
            case .failure(let error):
                print("Failed to get conversations: \(error)")
                break
            }
            
            
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
      
        
    }
   
    
    private func setupTableView() {
        tableView.register(UINib.init(nibName: "ConversationsTableViewCell", bundle: nil), forCellReuseIdentifier: "ConversationsTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
    }
    
   @objc func didTapComposeButton(){
    let storyboard = UIStoryboard.init(name: "Chat", bundle: nil)
    let vc = storyboard.instantiateViewController(identifier: "NewconversationViewController") as! NewconversationViewController
    //vc.delegate = self
    vc.completion = {[weak self] result in
        print("\(result)")
       
        let conversations = self?.conversations
        if let targetConversation = conversations?.first(where:{
            $0.other_user_email == DatabaseManager.safeEmail(email: result.email) && $0.channel_type.lowercased() == "single"
            
        }){
            let name = targetConversation.name
            let email = targetConversation.other_user_email ?? ""
            let vc = ChatViewController.loadFromNib(with: email, id: targetConversation.id, isGroup: false)
            vc.isNEwConversation = false
            vc.title = name
            vc.navigationItem.largeTitleDisplayMode = .never
            self?.navigationController?.pushViewController(vc, animated: true)
        }else{
            self?.createNewConversation(result: result)
        }
        
    }
    
    vc.groupCompletion = { [weak self] groupData in
        print("group data")
        if let groupId = groupData.groupId {
            var members = [GroupMembers]()
            for member in groupData.members {
                members.append(GroupMembers.init(name: member.name, email: member.email, isAdmin: false))
            }
            let vc = ChatViewController.loadFromNib(with: nil, id: groupId, isGroup: true)
            vc.isNEwConversation = false
            vc.groupMembers = members
            vc.groupData = groupData
            vc.title = groupData.name
            vc.navigationItem.largeTitleDisplayMode = .never
            self?.navigationController?.pushViewController(vc, animated: true)
        }else {
            print("group is not created")
        }
        
        
    }
    let nav = UINavigationController.init(rootViewController: vc)
    nav.isModalInPresentation = true
    nav.navigationItem.largeTitleDisplayMode = .never
    nav.modalPresentationStyle = .fullScreen
    present(nav, animated: true, completion: nil)
    //navigationController?.pushViewController(vc, animated: true)
    }
    
    private func createNewConversation(result: SearchResult) {
        let name = result.name
        let email = result.email
        
        // check in Database if conversation with these two users exists
        //if it does, reuse conversation Id
        DatabaseManager.shared.conversationExistsWith(targetReciepentEmail: result.email) { [weak self] result in
            
            switch result {
            
            case .success(let conversationId):
                // otherwise Create new Conversation
                let vc = ChatViewController.loadFromNib(with: email, id: conversationId, isGroup: false)
                vc.isNEwConversation = false
                vc.title = name
                vc.navigationItem.largeTitleDisplayMode = .never
                self?.navigationController?.pushViewController(vc, animated: true)
            case .failure(_):
                let vc = ChatViewController.loadFromNib(with: email, id: nil, isGroup: false)
                vc.isNEwConversation = true
                vc.title = name
                vc.navigationItem.largeTitleDisplayMode = .never
                self?.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
    }
    /*private func checkIfGroupConversationExist(result: GroupData) {
        let name = result.name
        let email = result.email
        
        // check in Database if conversation with these two users exists
        //if it does, reuse conversation Id
        DatabaseManager.shared.conversationExistsWith(targetReciepentEmail: result.email) { [weak self] result in
            
            switch result {
            
            case .success(let conversationId):
                // otherwise Create new Conversation
                let vc = ChatViewController(with: email, id: conversationId)
                vc.isNEwConversation = false
                vc.title = name
                vc.navigationItem.largeTitleDisplayMode = .never
                self?.navigationController?.pushViewController(vc, animated: true)
            case .failure(_):
                let vc = ChatViewController(with: email, id: nil)
                vc.isNEwConversation = true
                vc.title = name
                vc.navigationItem.largeTitleDisplayMode = .never
                self?.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
    }*/

}


extension ConversationsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return conversations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let oneRecord = conversations[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConversationsTableViewCell", for: indexPath) as! ConversationsTableViewCell
        
        cell.configure(with: oneRecord)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let model = conversations[indexPath.row]
        openConversation(model: model)
        
    }
    
    func openConversation(model: Conversation){
        let channel_type = model.channel_type
        if channel_type.lowercased() == "group" {
            //let userEmail = nil
            let members = model.members
            let vc = ChatViewController.loadFromNib(with: nil, id: model.id, isGroup: true)
            vc.title = model.name
            vc.groupMembers = members
            vc.isNEwConversation = false
            vc.navigationItem.largeTitleDisplayMode = .never
            navigationController?.pushViewController(vc, animated: true)
        }else {
            let userEmail = model.other_user_email ?? ""
            let vc = ChatViewController.loadFromNib(with: userEmail, id: model.id, isGroup: false)
            vc.title = model.name
            vc.navigationItem.largeTitleDisplayMode = .never
            navigationController?.pushViewController(vc, animated: true)
        }
       
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
             // begin delete
            let conversationId = conversations[indexPath.row].id
            tableView.beginUpdates()
            DatabaseManager.shared.deleteConversation(conversationId: conversationId) { [weak self] success in
                if success {
                    guard let count = self?.conversations.count else {return}
                    if count-1 >= indexPath.row {
                        self?.conversations.remove(at: indexPath.row)
                    }
                    tableView.deleteRows(at: [indexPath], with: .fade)
                    tableView.endUpdates()
                }
            }
            
            
            
        }
    }
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
}

extension ConversationsViewController: NewConversationDelegate {
    
    func createOneToOneConversationWith(user: SearchResult) {
       
            let conversations = conversations
            if let targetConversation = conversations.first(where:{
                $0.other_user_email == DatabaseManager.safeEmail(email: user.email)
                
            }){
                let name = targetConversation.name
                let email = targetConversation.other_user_email ?? ""
                let vc = ChatViewController.loadFromNib(with: email, id: targetConversation.id, isGroup: false)
                vc.isNEwConversation = false
                vc.title = name
                vc.navigationItem.largeTitleDisplayMode = .never
                navigationController?.pushViewController(vc, animated: true)
            }else{
                createNewConversation(result: user)
            }
            
        
    }
    
    func createGroupConversationWith(users: [SearchResult], groupName: String, groupImage: Data?) {
        
    }
    
    
}

// MARK: Test
extension ConversationsViewController {
    
    func dateSort(){
        
        // The default timeZone for ISO8601DateFormatter is UTC
        let utcISODateFormatter = ISO8601DateFormatter()

        // Printing a Date
        let date = Date()
        let utc_dateString = utcISODateFormatter.string(from: date)
        print(utc_dateString)
        // The default timeZone on ISO8601DateFormatter is UTC.
        // Set timeZone to UTTimeZone.current to get local time.
        let localISOFormatter = ISO8601DateFormatter()
        localISOFormatter.timeZone = TimeZone.current
        
        // Printing a Date
        //let utc_dateString = "2020-05-30T23:32:27-05:00"
        let localDate = localISOFormatter.date(from: utc_dateString)
        print(localDate)
        print("local dateString: \(utc_dateString.toLocalDateString())")
        var uploadTimes = ["02.02.2002 10:15:05","03.02.2002 08:12:00","05.02.2002 12:12:12","04.02.2002 13:14:13", "04.02.2002 01:14:13"]
         //uploadTimes = uploadTimes.map{ $0.components(separatedBy: ".").reversed().joined(separator: ".")}
        // print(uploadTimes)
          //["2002.02.02", "2002.02.03", "2002.02.05", "2002.02.04"]


         let isoFormatter = ISO8601DateFormatter()
        isoFormatter.formatOptions = [.withInternetDateTime,.withDashSeparatorInDate,.withColonSeparatorInTime,.withColonSeparatorInTimeZone]
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy HH:mm:ss"

         let dates = uploadTimes.compactMap { dateFormatter.date(from: $0) }
        // print(dates)
         let sortedDates = dates.sorted { $0 > $1 }
         print(sortedDates)
    }
    
   
    func newDateSort() -> Void {
        let uploadTimes = [Post(title: "", date: "05.09.2018", user: ""),
                       Post(title: "Hello", date: "02.10.2018", user: "Roman"),
                       Post(title: "Hi", date: "01.10.2018", user: "Roman"),
                       ]

        //sort dates
        let dates = uploadTimes.compactMap { $0.dateFromString }
        //print(dates)
        let sortedDates = dates.sorted { $0 > $1 }
        //print(sortedDates)

        //sort posts
        let sortedPost = uploadTimes.sorted{ $0.dateFromString > $1.dateFromString  }
        print("Sort Posts",sortedPost)
    }
    
}

struct Post {
var title: String
var date: String
var user: String
}

extension Post{
static  let isoFormatter : ISO8601DateFormatter = {
    let formatter =  ISO8601DateFormatter()
        formatter.formatOptions = [.withFullDate,]
    return formatter
   }()

var dateFromString : Date  {
    let  iSO8601DateString = date.components(separatedBy: ".").reversed().joined(separator: ".")
    return  Post.isoFormatter.date(from: iSO8601DateString)!
}
}
