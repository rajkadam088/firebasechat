//
//  ProfileViewController.swift
//  FirebaseChat
//
//  Created by Raj Kadam on 20/05/21.
//

import UIKit
import FirebaseAuth
import SDWebImage
class ProfileViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var data = [ProfileViewModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        data.append(ProfileViewModel.init(viewModelType: .info, title: "Name: \(SharedPref.shared.getFullName() ?? "Name")" , handler: nil))
        data.append(ProfileViewModel.init(viewModelType: .info, title: "Email: \(SharedPref.shared.getemail() ?? "email")" , handler: nil))
        data.append(ProfileViewModel.init(viewModelType: .info, title: "Log out", handler: { [weak self] in
            self?.logout()
        }))
        tableView.register(UINib.init(nibName: "ProfileTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableHeaderView = createTableHeder()
        // Do any additional setup after loading the view.
    }
    
    
    func createTableHeder() -> UIView? {
        guard let email =  SharedPref.shared.getemail() else {
            
            return nil
        }
        let safeEmail = DatabaseManager.safeEmail(email: email)
        let fileName = safeEmail+UserConstants.profilePicFileNameExtension
        let path = "images/"+fileName
        let headerView = UIView(frame: CGRect.init(x: 0, y: 0, width: tableView.width, height: 300))
        headerView.backgroundColor = .white
        let imageView = UIImageView.init(frame: CGRect.init(x:  (headerView.width-150)/2, y: 75, width: 150, height: 150))
        
        imageView.contentMode = .scaleAspectFill
        imageView.layer.borderColor = UIColor.black.cgColor
        imageView.backgroundColor = .white
        imageView.layer.borderWidth = 1.5
        imageView.layer.cornerRadius = imageView.width/2
        imageView.layer.masksToBounds = true
        headerView.addSubview(imageView)
        
        if let imageUrl = SharedPref.shared.getProfileImage() {
            imageView.sd_setImage(with: imageUrl, placeholderImage: UIImage.init(named: ""), options: .progressiveLoad, completed: nil)
        }else{
            StorageManager.shared.downloadURL(for: path) { result in
                switch result {
                case .success(let url):
                    SharedPref.shared.setProfileImage(url: url)
                    imageView.sd_setImage(with: url, placeholderImage: UIImage.init(named: ""), options: .progressiveLoad, completed: nil)
                    break
                case .failure(let error):
                    print("Failed to get download url: \(error)")
                    break
                }
            }
        }
        
        return headerView
    }

   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell", for: indexPath) as! ProfileTableViewCell
        cell.configureCellWith(viewModel: data[indexPath.row])
        return cell
    }
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 300
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        data[indexPath.row].handler?()
        
    }
    
    
    func logout(){
        let alert = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Log Out", style: .destructive, handler: { _ in
            do {
                try FirebaseAuth.Auth.auth().signOut()
                SharedPref.shared.deleteAll()
                Router.gotoLogin()
            }catch {
                print("Log out error")
            }
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        present(alert, animated: true, completion: nil)
        
    }
}
