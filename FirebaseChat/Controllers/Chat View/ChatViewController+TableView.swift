//
//  ChatViewController+TableView.swift
//  FirebaseChat
//
//  Created by Raj Kadam on 18/06/21.
//

import Foundation
import UIKit
extension ChatViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1 //messages.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //guard let conversations = messages[section].conversations else {return 0}
        
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
            let oneRecord = messages[indexPath.row]
            
            let type = getCellType(message: oneRecord)
            let isSent = getSendReceive(message: oneRecord)
            switch type {
             
            case .announcement:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "SystemTextTableViewCell", for: indexPath) as! SenderTextTableViewCell
                
                //cell.setupCell(data: oneRecord)
                cell.uiInitilization()
                
                return cell
                
                
            case .text:
                if isSent {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "SenderTextTableViewCell", for: indexPath) as! SenderTextTableViewCell
                    
                    cell.setupCell(message: oneRecord)
                    cell.uiInitilization()
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverTextTableViewCell", for: indexPath) as! ReceiverTextTableViewCell
                    
                    cell.setupCell(message: oneRecord)
                    cell.uiInitilization()
                    
                    return cell
                }
            
                
            case .photo:
                // check sender / receiver
                if isSent {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SenderImageMsgTableViewCell", for: indexPath) as! SenderImageMsgTableViewCell
                
                cell.setupCell(message: oneRecord)
                cell.uiInitilization()
                return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverImageTableViewCell", for: indexPath) as! ReceiverImageTableViewCell
                    
                    cell.setupCell(message: oneRecord)
                    cell.uiInitilization()
                    return cell
                }
            case .video:
                if isSent {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SenderVideoTableViewCell", for: indexPath) as! SenderVideoTableViewCell
                
                cell.setupCell(message: oneRecord)
                cell.uiInitilization()
                //cell.btnVideo.additinalText = oneRecord.content?.content_str ?? ""
                //cell.btnVideo.addTarget(self, action: #selector(videoClicked), for: .touchUpInside)
                return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverVideoMsgTableViewCell", for: indexPath) as! ReceiverVideoMsgTableViewCell
                
                    cell.setupCell(message: oneRecord)
                    cell.uiInitilization()
                    //cell.btnVideo.additinalText = oneRecord.content?.content_str ?? ""
                    //cell.btnVideo.addTarget(self, action: #selector(videoClicked), for: .touchUpInside)
                    return cell
                }
            
            case .location:
                let cell = tableView.dequeueReusableCell(withIdentifier: "SenderTextTableViewCell", for: indexPath) as! SenderTextTableViewCell
                
                cell.setupCell(message: oneRecord)
                cell.uiInitilization()
                return cell
                
            }
            
        
        
    }
    
    
    /*func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header_view = tableView.dequeueReusableCell(withIdentifier: "ConversationSectionHeaderTableViewCell") as! ConversationSectionHeaderTableViewCell
        
        
        if let date = messages[section].date {
            header_view.setupCell(date: date)
        }
        return header_view.contentView
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }*/
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let oneRecord = messages[indexPath.row]
        switch oneRecord.kind {
        case .photo(let media):
            guard let imageUrl = media.url else {
                return
            }
            
            let vc = PhotoViewerViewController.init(with: imageUrl)
            navigationController?.pushViewController(vc, animated: true)
        default:
            break
        }
        
    }
    
    func getCellType1(message: Message) -> String{
        return message.kind.messageKindString
    }
    
    func getSendReceive(message: Message) -> Bool {
        let sender_email = message.senderEmail
        guard let currentUserEmail = SharedPref.shared.getemail() else {
            return false
        }
        
        let safeEmail = DatabaseManager.safeEmail(email: currentUserEmail)
        if sender_email == safeEmail{
            return true
        }else{
            return false
        }
        
    }
    func getCellType(message: Message) -> MessageTypes{
        if message.kind.messageKindString.lowercased() == "text" {
            return MessageTypes.text
        }else if message.kind.messageKindString.lowercased() == "video" {
            return MessageTypes.video
        }else if message.kind.messageKindString.lowercased() == "photo" {
            return MessageTypes.photo
        } else if message.kind.messageKindString.lowercased() == "location" {
            return MessageTypes.location
        }else{
            return MessageTypes.announcement
        }
        
    }
}

enum MessageTypes {
    case text
    case photo
    case video
    case location
    case announcement
}
