//
//  ChatViewController.swift
//  FirebaseChat
//
//  Created by Raj Kadam on 22/05/21.
//

import UIKit
import AVKit
//import MessageKit
import GrowingTextView
import InputBarAccessoryView
import SDWebImage
 
class ChatViewController:  UIViewController{ //MessagesViewController

    var keyboardHeight: CGFloat = 0
    // Rk custom MessageController
    @IBOutlet weak var messageInputTextContainer: UIView!
    @IBOutlet weak var toolbarBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var messageInputTextView: GrowingTextView!
    
    //@IBOutlet weak var messageCollectionView: UICollectionView!
    @IBOutlet weak var messageTableView: UITableView!
    @IBOutlet weak var btnSend: UIButton!
    
    public static let  dateFormatter: ISO8601DateFormatter = {
       let formatter = ISO8601DateFormatter()
        //formatter.dateStyle = .medium
        //formatter.timeStyle = .long
        /*formatter.dateFormat = "dd/MM/yyyy HH:mm:ss a"
        formatter.locale = Locale.init(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone.init(identifier: "UTC")
         */
        return formatter
        
    }() 
    public var isNEwConversation: Bool = false
    private var isGroup: Bool = false
    public var groupData: GroupData?
    public var groupMembers: [GroupMembers]?
    public var otherUserEmail: String?
    var conversationId: String?
    var messages = [Message]()
    public var selfSender: Sender? {
        guard let email = SharedPref.shared.getemail() else {
            return nil
        }
        let safeEmail = DatabaseManager.safeEmail(email: email)
        let name = SharedPref.shared.getFullName() ?? "User"
        return Sender.init(photoURL: "", senderId: safeEmail, displayName: name)
    }
    
    /*init(with email: String?, id: String?, isGroup: Bool) {
        otherUserEmail = email
        conversationId = id
        self.isGroup = isGroup
        super.init(nibName: nil, bundle: nil)
        if let conversationId = conversationId {
            if isGroup {
                getGroupMembers()
            }
            listenForMessages(id: conversationId, shouldScrollToBottom: true)
        }
    }*/
    
    static func loadFromNib(with email: String?, id: String?, isGroup: Bool) -> ChatViewController{
        let stroyboard = UIStoryboard.init(name: "Chat", bundle: nil)
        let chat_vc = stroyboard.instantiateViewController(identifier: "ChatViewController") as! ChatViewController
        chat_vc.otherUserEmail = email
        chat_vc.conversationId = id
        chat_vc.isGroup = isGroup
        return chat_vc
    }
    
//    required init?(coder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInputTextView()
        if let conversationId = conversationId {
            listenForMessages(id: conversationId, shouldScrollToBottom: true)
        }
 
        if isGroup {
            getGroupMembers()
        }
        
        /*messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        messagesCollectionView.messageCellDelegate = self
        messageInputBar.delegate = self
        messagesCollectionView.backgroundColor = .lightGray*/

        inputBarUISetup()
        registerTableNibs()
        //setupInputBarButtons()
        setKeyboardObservers()
    }
    
    
    private func setupInputTextView(){
        //automaticallyAdjustsScrollViewInsets = false
        if #available(iOS 11.0, *) {
                messageTableView.contentInsetAdjustmentBehavior = .never
            messageInputTextView.contentInsetAdjustmentBehavior = .never
            } else {
                self.automaticallyAdjustsScrollViewInsets = false
            }
        messageInputTextView.delegate = self
        //messageInputTextView.maxLength = 440
        messageInputTextView.trimWhiteSpaceWhenEndEditing = false
        //messageInputTextView.placeholder = "Say something..."
        messageInputTextView.placeholderColor = UIColor(white: 0.8, alpha: 1.0)
        messageInputTextView.minHeight = 40.0
        messageInputTextView.maxHeight = 250.0
        messageInputTextView.backgroundColor = UIColor.white
        messageInputTextView.layer.cornerRadius = 4.0
    }
    private func setKeyboardObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(mykeyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(mykeyboardWillShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:
            UIResponder.keyboardDidHideNotification, object: nil)
    }
    
   
    
    /// get Group members
    func getGroupMembers(){
        guard let groupId = conversationId else {
            return
        }
        DatabaseManager.shared.getMembersFor(group: groupId) { [weak self] result in
            
            switch result {
            
            case .success(let members):
                self?.groupMembers = members
                print("Group members:\n \(members)")
            case .failure( let error):
                print("failed to fetch members: \(error)")
            }
        }
    }
    
    /// remove Avatars
//    func removeAvatar(){
//        if let layout = messagesCollectionView.collectionViewLayout as? MessagesCollectionViewFlowLayout {
//            layout.setMessageIncomingAvatarSize(.zero)
//            layout.setMessageOutgoingAvatarSize(.zero)
//        }
//    }
    /// register custom nibs
    
    func registerTableNibs() {
        
        self.messageTableView.register(UINib.init(nibName: "SenderTextTableViewCell", bundle: nil), forCellReuseIdentifier: "SenderTextTableViewCell")
        self.messageTableView.register(UINib.init(nibName: "SystemTextTableViewCell", bundle: nil), forCellReuseIdentifier: "SystemTextTableViewCell")
        self.messageTableView.register(UINib.init(nibName: "ReceiverTextTableViewCell", bundle: nil), forCellReuseIdentifier: "ReceiverTextTableViewCell")
        
        self.messageTableView.register(UINib.init(nibName: "ConversationSectionHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "ConversationSectionHeaderTableViewCell")
        
        self.messageTableView.register(UINib.init(nibName: "SenderImageMsgTableViewCell", bundle: nil), forCellReuseIdentifier: "SenderImageMsgTableViewCell")
        
        self.messageTableView.register(UINib.init(nibName: "ReceiverImageTableViewCell", bundle: nil), forCellReuseIdentifier: "ReceiverImageTableViewCell")
        self.messageTableView.register(UINib.init(nibName: "SystemContentApprovedMsgTableViewCell", bundle: nil), forCellReuseIdentifier: "SystemContentApprovedMsgTableViewCell")
        
        //ReceiverVideoMsgTableViewCell
        self.messageTableView.register(UINib.init(nibName: "ReceiverVideoMsgTableViewCell", bundle: nil), forCellReuseIdentifier: "ReceiverVideoMsgTableViewCell")
        self.messageTableView.register(UINib.init(nibName: "SenderVideoTableViewCell", bundle: nil), forCellReuseIdentifier: "SenderVideoTableViewCell")
        
        self.messageTableView.register(UINib.init(nibName: "EditRequestedTableViewCell", bundle: nil), forCellReuseIdentifier: "EditRequestedTableViewCell")
        
        self.messageTableView.register(UINib.init(nibName: "OfferQuoteReceiverTableViewCell", bundle: nil), forCellReuseIdentifier: "OfferQuoteReceiverTableViewCell")
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.navigationBar.layer.zPosition = 1
    }

    private func listenForMessages(id: String, shouldScrollToBottom: Bool) {
        DatabaseManager.shared.getAllMessagesForConversation(with: id) { [weak self] result in
            switch result {
            case .success(let messages):
                guard !messages.isEmpty else {
                    return
                }
                self?.messages = messages
                DispatchQueue.main.async {
                    //self?.messagesCollectionView.reloadDataAndKeepOffset()
                    self?.messageTableView.reloadData()
                    
                    if shouldScrollToBottom {
                        //self?.messagesCollectionView.scrollToLastItem()
                        self?.scrollToBottom()
                    }
                }
                /// Read all messages in conversation
                //self?.readAllMessages(id: id)
            case .failure(let error):
                print("Failed to get messages: \(error)")
                break
            }
        }
        
        
    }
    
    func readAllMessages(id: String){
        if id.hasPrefix("group"){
            print("its group conversation")
            DatabaseManager.shared.readGroupConversation(conversationId: id) { success in
                if success {
                    print("read messages successfully")
                }else{
                    print("read messages failed")
                }
            }
        }else{
            DatabaseManager.shared.readOneToOneConversation(conversationId: id) { success in
                if success {
                    print("read messages successfully")
                }else{
                    print("read messages failed")
                }
            }
        }
        
    }
}

// MARK: Send Message
extension ChatViewController {
    @IBAction func sendButtonTapped() {
        
        guard let text = messageInputTextView.text, !text.replacingOccurrences(of: " ", with: "").isEmpty, let selfSender = selfSender, let messageID = createMessageID(), let email = SharedPref.shared.getemail() else {
            return
        }
        
        let safeEmail = DatabaseManager.safeEmail(email: email)
        let name = SharedPref.shared.getFullName() ?? "User"
        print("Sending Text", text)
        // Send Message
        let message = Message.init(sender: selfSender, messageId: messageID,  sentDate: Date(), kind: .text(text), senderName: name, senderEmail: safeEmail, isRead: false)
        
        if isNEwConversation {
            // create group conversation in DB
            if isGroup {
                // create One to one conversation in DB
                guard let members = groupData?.members else {
                    print("group members are empty")
                    return
                }
                DatabaseManager.shared.createNewGroupConversation(with: members, groupName: self.title ?? "Group", firstMessage: message) { [weak self] created in
                    
                    if created {
                        print("message Sent")
                        self?.isNEwConversation = false
                        //inputBar.inputTextView.resignFirstResponder()
                        self?.messageInputTextView.text = ""
                        self?.btnSend.isHidden = true
                        let newConversationId = "conversation_\(message.messageId)"
                        self?.conversationId = newConversationId
                        self?.listenForMessages(id: newConversationId, shouldScrollToBottom: true)
                    }else{
                        print("Failed to Sent")
                    }
                }
            }else {
                // create One to one conversation in DB
                guard let otherUserEmail = otherUserEmail else {
                    print("other user email not found ")
                    return
                }
                DatabaseManager.shared.createNewConversation(with: otherUserEmail, name: self.title ?? "User", firstMessage: message) { [weak self] created in
                    
                    if created {
                        print("message Sent")
                        self?.isNEwConversation = false
                        //inputBar.inputTextView.resignFirstResponder()
                        self?.messageInputTextView.text = ""
                        self?.btnSend.isHidden = true
                        let newConversationId = "conversation_\(message.messageId)"
                        self?.conversationId = newConversationId
                        self?.listenForMessages(id: newConversationId, shouldScrollToBottom: true)
                    }else{
                        print("Failed to Sent")
                    }
                }
            }

        }else{
            // append to exiting DB
            guard let conversationId = conversationId, let name = self.title else {
                return
            }
            DatabaseManager.shared.sendMessageTo(convesationId: conversationId, otherUserEmail: otherUserEmail, name: name, newMessage: message, groupMembers: groupMembers) { [weak self] success in
                
                if success {
                    //inputBar.inputTextView.resignFirstResponder()
                    self?.messageInputTextView.text = ""
                    self?.btnSend.isHidden = true
                    print("message Sent")
                }else{
                    print("Failed to Sent in group")
                }
            }
        }
    }
}
extension ChatViewController: InputBarAccessoryViewDelegate {
    func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {
        guard !text.replacingOccurrences(of: " ", with: "").isEmpty, let selfSender = selfSender, let messageID = createMessageID(), let email = SharedPref.shared.getemail() else {
            return
        }
        
        let safeEmail = DatabaseManager.safeEmail(email: email)
        let name = SharedPref.shared.getFullName() ?? "User"
        print("Sending Text", text)
        // Send Message
        let message = Message.init(sender: selfSender, messageId: messageID,  sentDate: Date(), kind: .text(text), senderName: name, senderEmail: safeEmail, isRead: false)
        
        if isNEwConversation {
            // create group conversation in DB
            if isGroup {
                // create One to one conversation in DB
                guard let members = groupData?.members else {
                    print("group members are empty")
                    return
                }
                DatabaseManager.shared.createNewGroupConversation(with: members, groupName: self.title ?? "Group", firstMessage: message) { [weak self] created in
                    
                    if created {
                        print("message Sent")
                        self?.isNEwConversation = false
                        //inputBar.inputTextView.resignFirstResponder()
                        inputBar.inputTextView.text = ""
                        let newConversationId = "conversation_\(message.messageId)"
                        self?.conversationId = newConversationId
                        self?.listenForMessages(id: newConversationId, shouldScrollToBottom: true)
                    }else{
                        print("Failed to Sent")
                    }
                }
            }else {
                // create One to one conversation in DB
                guard let otherUserEmail = otherUserEmail else {
                    print("other user email not found ")
                    return
                }
                DatabaseManager.shared.createNewConversation(with: otherUserEmail, name: self.title ?? "User", firstMessage: message) { [weak self] created in
                    
                    if created {
                        print("message Sent")
                        self?.isNEwConversation = false
                        //inputBar.inputTextView.resignFirstResponder()
                        inputBar.inputTextView.text = ""
                        let newConversationId = "conversation_\(message.messageId)"
                        self?.conversationId = newConversationId
                        self?.listenForMessages(id: newConversationId, shouldScrollToBottom: true)
                    }else{
                        print("Failed to Sent")
                    }
                }
            }

        }else{
            // append to exiting DB
            guard let conversationId = conversationId, let name = self.title else {
                return
            }
            DatabaseManager.shared.sendMessageTo(convesationId: conversationId, otherUserEmail: otherUserEmail, name: name, newMessage: message, groupMembers: groupMembers) { success in
                
                if success {
                    //inputBar.inputTextView.resignFirstResponder()
                    inputBar.inputTextView.text = ""
                    print("message Sent")
                }else{
                    print("Failed to Sent")
                }
            }
        }
    }
    
    func createMessageID() -> String? {
        // date, otherUserEmail, SenderEmail, randomInt
        let dateString = Self.dateFormatter.string(from: Date()).safeDate()
        guard let currentUserEmail = SharedPref.shared.getemail(), let groupName = self.title  else {
            return nil
        }
        let otherUserEmail = otherUserEmail ?? groupName
        let safeEmail = DatabaseManager.safeEmail(email: currentUserEmail)
        let newIdentifier = "\(otherUserEmail)_\(safeEmail)_\(dateString)"
        
        print("Created message id: \(newIdentifier)")
        return newIdentifier
    }
    
   
}


/*extension ChatViewController: MessagesDataSource, MessagesLayoutDelegate, MessagesDisplayDelegate{
    
    func currentSender() -> SenderType {
        if let sender = selfSender {
            return sender
        }
        fatalError("Self sendr is nil, email should be cached")
        //return Sender.init(photoURL: "", senderId: "", displayName: "")
    }
    
    
    /// set bubble size
    func messageStyle(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
        let sender = message.sender
        if sender.senderId == currentSender().senderId {
            return .bubbleTail(.topRight, .pointedEdge)
        }else{
            return .bubbleTail(.topLeft, .pointedEdge)
        }
    }
    
    /// Detect special messages
    func enabledDetectors(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> [DetectorType] {
        return [.address, .phoneNumber, .url, .hashtag, .mention]
    }
    
    /// remove Avatar
    func configureAvatarView(_ avatarView: AvatarView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
            avatarView.isHidden = true
    }
    
    /// set message to cell
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return messages[indexPath.section]
    }
    
    /// number of sections
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messages.count
    }
   
    //. configure media cells
    func configureMediaMessageImageView(_ imageView: UIImageView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
        guard let message = message as? Message else {
            return
        }
        switch message.kind {
        case .photo( let media):
            guard let imageUrl = media.url else {
                return
            }
            imageView.sd_setImage(with: imageUrl, completed: nil)
        default:
            break
        }
    }
    
    /// set background color for  sender and receiver cells
    func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        let sender = message.sender
        if sender.senderId == currentSender().senderId {
            // our message
            return .link
        }else{
            // received message
            return .white
        }
    }
    
    /// set text color for  sender and receiver cells
    func textColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        let sender = message.sender
        if sender.senderId == currentSender().senderId {
            // our message
            return .white
        }else{
            // received message
            return .black
        }
    }
}

extension ChatViewController: MessageCellDelegate {
 
    func didTapImage(in cell: MessageCollectionViewCell) {
        guard let indexPath = messagesCollectionView.indexPath(for: cell) else {
            return
        }
        let message = messages[indexPath.section]
        
        switch message.kind {
        case .photo( let media):
            guard let imageUrl = media.url else {
                return
            }
            
            let vc = PhotoViewerViewController.init(with: imageUrl)
            navigationController?.pushViewController(vc, animated: true)
        case .video( let media):
            guard let videoUrl = media.url else {
                return
            }
            
            let vc = AVPlayerViewController()
            vc.player = AVPlayer.init(url: videoUrl)
            present(vc, animated: true, completion: nil)
        default:
            break
        }
    }
    func didTapMessage(in cell: MessageCollectionViewCell)
    {
        guard let indexPath = messagesCollectionView.indexPath(for: cell) else {
            return
        }
        let message = messages[indexPath.section]
        
        switch message.kind {
        case .location( let locationData):
            let coordinaes = locationData.location.coordinate
            
            let storyboard = UIStoryboard.init(name: "Chat", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "LocationPickerViewController") as LocationPickerViewController
            vc.isPickable = false
            vc.selectedCoordinates = coordinaes
            navigationController?.pushViewController(vc, animated: true)
       
        default:
            break
        }
    }
}
*/

