//
//  ChatViewController+InputBar.swift
//  FirebaseChat
//
//  Created by Raj Kadam on 18/06/21.
//

import Foundation
import UIKit
import GrowingTextView
extension ChatViewController {
    
    func inputBarUISetup(){
        messageInputTextContainer.layer.cornerRadius = 20
        messageInputTextContainer.layer.borderWidth = 1
        messageInputTextContainer.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func scrollToBottom(){
        print(self.messageTableView.contentSize.height)
        print(self.messageTableView.contentSize.width)
        print(self.messageTableView.contentOffset)
        
        let screenHeight = UIScreen.main.bounds.size.height
        
        let visibleArea = screenHeight-keyboardHeight
        let lastContentPositionOnScreen = self.messageTableView.frame.origin.y + self.messageTableView.contentSize.height
        
        
        if visibleArea > lastContentPositionOnScreen {
            print("visible area: \(visibleArea) \nContentPos: \(lastContentPositionOnScreen)")
            return
        }
        
        
        // Get Current Indexpath
        let lastSection = messageTableView.numberOfSections - 1
        
       
        
        //let item = self.messageCollectionView.numberOfItems(inSection: lastSection)-1
        
        let item = self.messageTableView.numberOfRows(inSection: lastSection)-1
        let lastItemIndex = NSIndexPath(item: item, section: lastSection) as IndexPath
        
        self.messageTableView.scrollToRow(at: lastItemIndex, at: .bottom, animated: false)
        //self.messageCollectionView.scrollToItem(at: lastItemIndex, at: .top, animated: false)
        
    }
    
    func scrollToTop(){

        let firstItemIndex = NSIndexPath(item: 0, section: 0) as IndexPath
        
        self.messageTableView.scrollToRow(at: firstItemIndex, at: .top, animated: false)
        
    }
    @objc func mykeyboardWillShow(_ notification:Notification?) {
        
        let deviceType = DeviceType().getDeviceType()
        
        
        
        if notification != nil {
            if let keyboardFrame: NSValue = notification?.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
                let keyboardRectangle = keyboardFrame.cgRectValue
                if (notification?.name == UIResponder.keyboardWillShowNotification || notification?.name == UIResponder.keyboardDidShowNotification) {
                    keyboardHeight = keyboardRectangle.height
                }
            }
        } else {
            keyboardHeight = 0.0
        }
        if deviceType == .iphoneX || deviceType == .iphoneX_series{
            keyboardHeight = keyboardHeight-32
        }
        
        UIView.animate(withDuration: 0.2, delay: 0, options: .transitionFlipFromBottom,animations: {
            
            self.toolbarBottomConstraint.constant =  -(self.keyboardHeight)
            self.scrollToBottom()
            self.view.layoutIfNeeded()
            
        }) { (flag) in
            
            
            
        }
        
        
    }
    
    @objc func keyboardWillHide(_ notification:Notification) {
        
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            self.toolbarBottomConstraint.constant =  0
        }
    }
}

// MARK: Growing TextView
extension ChatViewController:  GrowingTextViewDelegate {
    
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        messageInputTextView.resignFirstResponder()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        
        var inputText = messageInputTextView.text ?? ""
        inputText = inputText.trimmingCharacters(in: .whitespacesAndNewlines)//replacingOccurrences(of: " ", with: "")
        //inputText = inputText.replacingOccurrences(of: "\n", with: "")
        
        if inputText.count == 0 { //&& text.count < 2
            btnSend.isHidden = true
            let range = text.rangeOfCharacter(from: NSCharacterSet.whitespacesAndNewlines)
            //string.rangeOfCharacterFromSet(NSCharacterSet.whitespaceCharacterSet)
            if range != nil || text.count == 0{
                print("whitespace found")
                btnSend.isHidden = true
                return true
            }else {
                
                if text.count > 0 {
                    btnSend.isHidden = false
                    return true
                }
                btnSend.isHidden = true
                print("whitespace not found")
                return true
            }
        }
        else if inputText.count == 1 {
            
            if text == "" {
                let updatedText = String(textView.text.dropLast())
                if updatedText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                    btnSend.isHidden = true
                    return true
                }else{
                   return true
                }
               
            }
            
        }
        btnSend.isHidden = false
        return true
    }
    
    
    
}

extension ChatViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        messageInputTextView.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField.text?.count == 0 && string.count < 2{
            btnSend.isHidden = true
            let range = string.rangeOfCharacter(from: NSCharacterSet.whitespaces)
            //string.rangeOfCharacterFromSet(NSCharacterSet.whitespaceCharacterSet)
            if range != nil {
                print("whitespace found")
                btnSend.isHidden = true
                return false
            }else {
                if string.count > 0 {
                    btnSend.isHidden = false
                }
                btnSend.isHidden = true
                print("whitespace not found")
            }
        }
        if textField.text?.count == 1 {
            if string == "" {
                btnSend.isHidden = true
                return true
            }
            
        }
        btnSend.isHidden = false
        return true
    }
}
