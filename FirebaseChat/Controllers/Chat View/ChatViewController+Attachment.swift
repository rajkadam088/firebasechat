//
//  ChatViewController+Attachment.swift
//  FirebaseChat
//
//  Created by Raj Kadam on 24/05/21.
//

import Foundation
import UIKit
import MessageKit
import InputBarAccessoryView
import CoreLocation
// MARK: -Attachment
extension ChatViewController{
    
    /*func setupInputBarButtons(){
        let attachment = InputBarButtonItem()
        attachment.setSize(CGSize.init(width: 35, height: 35), animated: false)
        attachment.setImage(UIImage.init(systemName: "plus.circle"), for: .normal)
        attachment.tintColor = .black
        attachment.onTouchUpInside { [weak self] button in
            self?.presentInputAttachmentSheet()
        }
        messageInputBar.setLeftStackViewWidthConstant(to: 36, animated: false)
        messageInputBar.setStackViewItems([attachment], forStack: .left, animated: false)
    }*/
  
    @IBAction private func presentInputAttachmentSheet(){
        
        let actionSheet = UIAlertController.init(title: "Attach Media", message: "What would you like to attach?", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction.init(title: "Photo", style: .default, handler: { [weak self] _ in
            self?.presentPhotoActionSheet()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Video", style: .default, handler: { [weak self] _ in
            self?.presentVideoActionSheet()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Audio", style: .default, handler: { [weak self] _ in
            //self?.presentPhotoPicker()
        }))
        
        actionSheet.addAction(UIAlertAction.init(title: "Location", style: .default, handler: { [weak self] _ in
            self?.presentLocationPicker()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        present(actionSheet, animated: true, completion: nil)
    }
    
    private func presentLocationPicker(){
        let storyboard = UIStoryboard.init(name: "Chat", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "LocationPickerViewController") as LocationPickerViewController
        vc.navigationItem.largeTitleDisplayMode = .never
        vc.completion = { [weak self] coordinates in
            let longitude: Double = coordinates.longitude
            let latitude: Double = coordinates.latitude
            self?.sendLocationMessage(coordinates: coordinates)
            print("long:", longitude)
            print("lati:", latitude)
            //
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    private func presentPhotoActionSheet(){
        let actionSheet = UIAlertController.init(title: "Attach Photo", message: "Where would you like to attach a photo from?", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction.init(title: "Camera", style: .default, handler: { [weak self] _ in
            self?.presentCamera()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Photo Library", style: .default, handler: { [weak self] _ in
            self?.presentPhotoPicker()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        present(actionSheet, animated: true, completion: nil)
    }
    
    private func presentVideoActionSheet(){
        let actionSheet = UIAlertController.init(title: "Attach Video", message: "Where would you like to attach a video from?", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction.init(title: "Camera", style: .default, handler: { [weak self] _ in
            let vc = UIImagePickerController()
            vc.sourceType = .camera
            vc.mediaTypes = ["public.movie"]
            vc.videoQuality = .typeMedium
            vc.delegate = self
            vc.allowsEditing = true
            self?.present(vc, animated: true, completion: nil)
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Library", style: .default, handler: { [weak self] _ in
            self?.presentVideoPicker()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        present(actionSheet, animated: true, completion: nil)
    }
    
    func presentCamera(){
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.delegate = self
        vc.allowsEditing = true
        present(vc, animated: true, completion: nil)
    }
    
    func presentPhotoPicker(){
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        vc.delegate = self
        vc.allowsEditing = true
        present(vc, animated: true, completion: nil)
    }
    
    func presentVideoPicker(){
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        vc.mediaTypes = ["public.movie"]
        vc.videoQuality = .typeMedium
        vc.delegate = self
        vc.allowsEditing = true
        present(vc, animated: true, completion: nil)
    }
}

extension ChatViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let messageID = createMessageID(), let conversationID = conversationId, let name = self.title, let selfSender = selfSender else{
            picker.dismiss(animated: true, completion: nil)
            return
        }
        
        if let image = info[.editedImage] as? UIImage, let imageData = image.jpegData(compressionQuality: 0.7) {
            // photo picked
            sendePhotoAttachment(messageID: messageID, conversationID: conversationID, selfSender: selfSender, imageData: imageData, name: name)
        }else if let videoUrl = info[.mediaURL] as? URL{
            // video picked
            sendVideoAttachment(messageID: messageID, conversationID: conversationID, selfSender: selfSender, videoURL: videoUrl, name: name)
        }
        
        // send message
        
        picker.dismiss(animated: true, completion: nil)
    }
    
   
    
    
}

// MARK - Media Messages
extension ChatViewController {
    // MARK: - Send Video Attachment
    private func sendVideoAttachment(messageID: String, conversationID: String, selfSender: Sender, videoURL: URL, name: String){
        let fileName = "video_message_" + messageID.replacingOccurrences(of: " ", with: "-") + ".mov"
        
        // 1. upload video to storage
        // 2. Send message with type video
        
        /// 1. upload video to storage
        StorageManager.shared.uploadVideoForMessage(with: videoURL, fileName: fileName) { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            switch result {
            case .success(let urlString):
                print("message video uploaded : \(urlString)")
                guard let url = URL.init(string: urlString), let placeholder = UIImage.init(systemName: "plus"), let email = SharedPref.shared.getemail() else {
                    return
                }
                let media = Media.init(url: url,
                                       image: nil,
                                       placeholderImage: placeholder,
                                       size: .zero)
                let safeEmail = DatabaseManager.safeEmail(email: email)
                let name = SharedPref.shared.getFullName() ?? "User"
                
                let message = Message.init(sender: selfSender,
                                           messageId: messageID,
                                           sentDate: Date(),
                                           kind: .video(media), senderName: name, senderEmail: safeEmail, isRead: false)
                
                ///2. Send message with type video
                DatabaseManager.shared.sendMessageTo(convesationId: conversationID, otherUserEmail: strongSelf.otherUserEmail, name: name, newMessage: message, groupMembers: strongSelf.groupMembers) { [weak self] success in
                    if success {
                       
                       // self?.messageInputBar.inputTextView.text = ""
                        //self?.messageInputText.text = ""
                        print("video message Sent")
                    }else{
                        print("Failed to Send video")
                    }
                }
            case .failure(let error):
                print("message video upload error: \(error)")
            }
        }
    }
    
    // MARK: - Send Photo Attachment
    private func sendePhotoAttachment(messageID: String, conversationID: String, selfSender: Sender, imageData: Data, name: String){
        let fileName = "photo_message_" + messageID.replacingOccurrences(of: " ", with: "-") + ".png"
        
        // 1. upload image to storage
        // 2. Send message with type photo
        
        /// 1. upload image to storage
        StorageManager.shared.uploadPictureForMessage(with: imageData, fileName: fileName) { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            switch result {
            case .success(let urlString):
                print("message photo uploaded : \(urlString)")
                guard let url = URL.init(string: urlString), let placeholder = UIImage.init(systemName: "plus"), let email = SharedPref.shared.getemail() else {
                    return
                }
                let media = Media.init(url: url,
                                       image: nil,
                                       placeholderImage: placeholder,
                                       size: .zero)
                let safeEmail = DatabaseManager.safeEmail(email: email)
                let name = SharedPref.shared.getFullName() ?? "User"
                
                let message = Message.init(sender: selfSender,
                                           messageId: messageID,
                                           sentDate: Date(),
                                           kind: .photo(media), senderName: name, senderEmail: safeEmail, isRead: false)
                
                ///2. Send message with type photo
                DatabaseManager.shared.sendMessageTo(convesationId: conversationID, otherUserEmail: strongSelf.otherUserEmail, name: name, newMessage: message, groupMembers: strongSelf.groupMembers) { [weak self] success in
                    if success {                        //self?.messageInputBar.inputTextView.text = ""
                        //self?.messageInputText.text = ""
                        print("photo message Sent")
                    }else{
                        print("Failed to Send photo")
                    }
                }
            case .failure(let error):
                print("message photo upload error: \(error)")
            }
        }
    }
    
    
    func sendLocationMessage(coordinates: CLLocationCoordinate2D){
        let locationItem = Location.init(location: CLLocation.init(latitude: coordinates.latitude, longitude: coordinates.longitude), size: .zero)
        
        guard let messageID = createMessageID(), let conversationID = conversationId, let name = self.title, let selfSender = selfSender, let email = SharedPref.shared.getemail()  else {
            return
        }
        let safeEmail = DatabaseManager.safeEmail(email: email)
        let sender_name = SharedPref.shared.getFullName() ?? "User"
        
        let message = Message.init(sender: selfSender,
                                   messageId: messageID,
                                   sentDate: Date(),
                                   kind: .location(locationItem), senderName: sender_name, senderEmail: safeEmail, isRead: false)
        
        ///2. Send message with type photo
        DatabaseManager.shared.sendMessageTo(convesationId: conversationID, otherUserEmail: otherUserEmail, name: name, newMessage: message, groupMembers: groupMembers) { [weak self] success in
            if success {
                //self?.messageInputBar.inputTextView.text = ""
                //self?.messageInputText.text = ""
                print("location message Sent")
            }else{
                print("Failed to Send location")
            }
        }
    }
}
