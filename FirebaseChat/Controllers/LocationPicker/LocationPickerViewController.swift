//
//  LocationPickerViewController.swift
//  FirebaseChat
//
//  Created by Raj Kadam on 26/05/21.
//

import UIKit
import CoreLocation
import MapKit

class LocationPickerViewController: UIViewController {

    @IBOutlet weak var map: MKMapView!
    var isPickable = true
    var selectedCoordinates: CLLocationCoordinate2D?
    public var completion: ((CLLocationCoordinate2D) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isPickable {
            title = "Pick Location"
        navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "Send", style: .done, target: self, action: #selector(sendButtonTapped))
        // Do any additional setup after loading the view.
       
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(didTapMap(_:)))
        tapGesture.numberOfTapsRequired = 1
        
        map.addGestureRecognizer(tapGesture)
            map.zoomToUserLocation()
        }else{
            title = "Location"
            // drop a pin on that location
            if let coordinates = selectedCoordinates {
                let pin = MKPointAnnotation()
                pin.coordinate = coordinates
                map.addAnnotation(pin)
                map.zoomToLocation(location: coordinates)
                
            }
        }
    }
    
    @objc func didTapMap(_ gesture: UITapGestureRecognizer)  {
        let locationInView = gesture.location(in: map)
        selectedCoordinates = map.convert(locationInView, toCoordinateFrom: map)
        
        for anotation in map.annotations {
            map.removeAnnotation(anotation)
        }
        
        // drop a pin on that location
        let pin = MKPointAnnotation()
        pin.coordinate = selectedCoordinates!
        map.addAnnotation(pin)
        
    }
    @objc func sendButtonTapped()  {
        guard let coordinates = selectedCoordinates else {
            return
        }
        completion?(coordinates)
        navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
