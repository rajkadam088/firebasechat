//
//  Device.swift
//  FirebaseChat
//
//  Created by Raj Kadam on 18/06/21.
//

import Foundation
import UIKit
enum Device {
    
    case iphone8
    case iphone5
    case iphoneX
    case iphoneX_series
    case iphone7Plus
    case iphone6SPlus
    case iPad
    
}
struct DeviceType{
    
    
    
    func getDeviceType() -> Device {
        
        if UIDevice().userInterfaceIdiom == .phone {
            let height = UIScreen.main.nativeBounds.height
            switch height {
            case 1136:
                return .iphone5
            case 1334:
                print("iPhone 6/6S/7/8")
                return .iphone8
            case 1920:
                print("iPhone 6+/6S+/7+/8+")
                return .iphone6SPlus
                
            case 1792:
                return .iphoneX_series
                
            case 2688:
                
                return .iphoneX_series
            case 2436:
                print("iPhone X")
                return .iphoneX
                
            
            default:
                if height > 2210 {
                print("iphone x series")
                return .iphoneX_series
                }else {
                    return .iphone7Plus
                }
                
            }
        }
        return .iPad
    }
}
