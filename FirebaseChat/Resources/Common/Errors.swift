//
//  Errors.swift
//  FirebaseChat
//
//  Created by Raj Kadam on 24/05/21.
//

import Foundation

public enum StorageErrors: Error {
    case failedToUpload
    case failedToGetDownloadUrl
}

public enum DownloadErrors: Error {
    case failedToDownload
    case failedToGetDownloadUrl
}


/*
 func loadSoundFiles() {
     //sound library
     // /System/Library/Audio/UISounds/
     guard let soundDir = URL.init(string: "/System/Library/Audio/UISounds/") else { print("URL not found")
         return
     }
     do {
         let fileUrls = try FileManager.default.contentsOfDirectory(at: soundDir, includingPropertiesForKeys: nil)
         
         soundFileList = fileUrls.map{ $0.lastPathComponent}
     } catch {
         soundFileList = []
     }
     
     print("soundlist: ",soundFileList)
 }
 */
