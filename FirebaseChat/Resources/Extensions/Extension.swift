//
//  Extension.swift
//  FirebaseChat
//
//  Created by Raj Kadam on 20/05/21.
//

import Foundation
import UIKit

extension UIView {
    public var width: CGFloat {
        return frame.size.width
    }
    public var height: CGFloat {
        return frame.size.height
    }
    
    public var top: CGFloat {
        return frame.origin.y
    }
    public var bottom: CGFloat {
        return frame.size.height+frame.origin.y
    }
    
    public var left: CGFloat {
        return frame.origin.x
    }
    public var right: CGFloat {
        return frame.size.width+frame.origin.x
    }
}

extension Date {
    func isInSameDayOf(date: Date) -> Bool {
        return Calendar.current.isDate(self, inSameDayAs:date)
    }

}

extension String {
    func safeDate() -> String {
        
        let safeDate = self.replacingOccurrences(of: ":", with: "-")
        return safeDate
    }
    func toLocalDateString() -> String?{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        if let convertedDate = formatter.date(from: self) {
            formatter.dateFormat = "dd/MM/yyyy HH:mm:ss a"
            formatter.timeZone = .current
            //formatter.locale = .current
            return formatter.string(from: convertedDate)
        }else {
            return nil
        }
    }
    func trimingLeadingSpaces(using characterSet: CharacterSet = .whitespacesAndNewlines) -> String {
        guard let index = firstIndex(where: { !CharacterSet(charactersIn: String($0)).isSubset(of: characterSet) }) else {
            return self
        }
        
        return String(self[index...])
    }
    
    
}

extension Notification.Name {
    static let didLoginNotification = Notification.Name("didLoginNotification")
    
}
