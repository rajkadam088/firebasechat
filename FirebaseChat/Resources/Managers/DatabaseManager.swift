//
//  DatabaseManager.swift
//  FirebaseChat
//
//  Created by Raj Kadam on 21/05/21.
//

import Foundation
import FirebaseDatabase

/// Allows you to insert,  fetch, users, messages and media to Firebase Realtime Database
final class DatabaseManager {
    
    static let shared = DatabaseManager()
    private init(){}
    let database = Database.database().reference()
    
    static func safeEmail(email: String) -> String {
        
        var safeEmail = email.replacingOccurrences(of: ".", with: "-")
         safeEmail = safeEmail.replacingOccurrences(of: "@", with: "-")
        return safeEmail
    }
    
    
}

// MARK: - Fetch Data
extension DatabaseManager{
    
    public func getDataFor(path: String, completion: @escaping (Result<Any, Error>) -> Void) {
        database.child(path).observeSingleEvent(of: .value) { snapshot in
            
            guard let value = snapshot.value else {
                completion(.failure(DataBaseError.failedToFetch))
                return
            }
            completion(.success(value))
        }
    }
}

// MARK: - Account Managment
extension DatabaseManager {
    
    public func userExists(with email: String,
                           compleaton: @escaping ((Bool) -> Void)) {
        
        let safeEmail = DatabaseManager.safeEmail(email: email)
        
        database.child("\(FolderName.Users)/\(safeEmail)").observeSingleEvent(of: .value) { DBSnapshot in
            guard let _ = DBSnapshot.value as? [String: Any] else {
                compleaton(false)
                return
            }
            
            compleaton(true)
        } withCancel: { error in
            
        }

    }
    
    /// Insert new user to database
    public func insertUser(with user: ChatAppUser, completion: @escaping (Bool) -> Void){
        
        database.child("\(FolderName.Users)/\(user.safeEmail)").setValue([UserDatakey.firstName: user.firstName,
            UserDatakey.lastName: user.lastName
        ]) { [weak self] error, reference in
            
            guard let strongSelf = self else {
                return
            }
            guard error == nil else {
                print("Failed to write to Database")
                completion(false)
                return
            }
            
            strongSelf.database.child("\(FolderName.SearchChannels)").observeSingleEvent(of: .value) { [weak self] snapshot in
                if var usersCollection = snapshot.value as? [[String: String]] {
                    // append to user dictionary
                    let newRecord: [String: String] = [UserDatakey.name: user.firstName+" "+user.lastName,
                                     UserDatakey.email: user.safeEmail
                                    ]
                    usersCollection.append(newRecord)
                    
                    self?.database.child("\(FolderName.SearchChannels)").setValue(usersCollection) { error, dbReference in
                        guard error == nil else {
                            completion(false)
                            return
                        }
                        completion(true)
                    }
                }else{
                    
                    //create that array
                    let newCollection: [[String: String]] = [
                        [UserDatakey.name: user.firstName+" "+user.lastName,
                         UserDatakey.email: user.safeEmail
                        ]
                    ]
                    
                    self?.database.child("\(FolderName.SearchChannels)").setValue(newCollection) { error, dbReference in
                        guard error == nil else {
                            completion(false)
                            return
                        }
                        completion(true)
                    }
                }
            }
           
        }
        
    }
    
    public func getAllUsers(completion: @escaping (Result<[[String: Any]], Error>) -> Void) {
        
        database.child("\(FolderName.SearchChannels)").observeSingleEvent(of:  .value) { snapshot in
            guard let usersCollection = snapshot.value as? [[String: Any]] else{
                completion(.failure(DataBaseError.failedToFetch))
                return
            }
            completion(.success(usersCollection))
        }
    }
    
    public enum DataBaseError: Error {
        case failedToFetch
    }
}

struct ChatAppUser {
    let firstName: String
    let lastName: String
    let emailAddress: String
    var safeEmail: String {
        var safeEmail = emailAddress.replacingOccurrences(of: ".", with: "-")
         safeEmail = safeEmail.replacingOccurrences(of: "@", with: "-")
        return safeEmail
    }
    
    var profilePictureFileName: String {
        // images/userEmail_profile_picture.png/
        return "\(safeEmail)_profile_picture.png"
    }
   
    
}

