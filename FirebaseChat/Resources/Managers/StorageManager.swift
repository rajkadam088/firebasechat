//
//  StorageManager.swift
//  FirebaseChat
//
//  Created by Raj Kadam on 22/05/21.
//

import Foundation
import FirebaseStorage

/// allows you to get , fetch, and upload files to Firebase storage
final class StorageManager {
    
    static let shared = StorageManager()
    
    private let storage = Storage.storage().reference()
    
    /*
     /images/userEmail_profile_picture.png/
     */
   public typealias UploadPictureCompletion = (Result<String,Error>) -> Void
    
    /// Uploads picture to firebase storage and returns completion with url string to downloads
    public func uploadProfilePicture(with data: Data, fileName: String, competion: @escaping UploadPictureCompletion) {
        
        storage.child("\(FolderName.ProfileImage)/\(fileName)").putData(data, metadata: nil) { [weak self] metaData, error in
            guard error == nil else{
                // failed
                print("Failed to upload data to firebase for picture")
                competion(.failure(StorageErrors.failedToUpload))
                return
            }
            
            self?.storage.child("\(FolderName.ProfileImage)/\(fileName)").downloadURL { url, error in
                guard let url = url else {
                    print("failed To Get Download Url")
                    competion(.failure(StorageErrors.failedToGetDownloadUrl))
                    return
                }
                
                let urlString = url.absoluteString
                print("download url return: \(urlString)")
                
                competion(.success(urlString))
            }
        }
    }
    
    /// Uploads picture that will be sent to a conversation message
    public func uploadPictureForMessage(with data: Data, fileName: String, competion: @escaping UploadPictureCompletion) {
        
        storage.child("\(FolderName.MessageImages)/\(fileName)").putData(data, metadata: nil) { [weak self] metaData, error in
            guard error == nil else{
                // failed
                print("Failed to upload data to firebase for picture")
                competion(.failure(StorageErrors.failedToUpload))
                return
            }
            
            self?.storage.child("\(FolderName.MessageImages)/\(fileName)").downloadURL { url, error in
                guard let url = url else {
                    print("failed To Get Download Url")
                    competion(.failure(StorageErrors.failedToGetDownloadUrl))
                    return
                }
                
                let urlString = url.absoluteString
                print("download url return: \(urlString)")
                
                competion(.success(urlString))
            }
        }
    }
    
    /// Uploads video that will be sent to a conversation message
    public func uploadVideoForMessage(with fileUrl: URL, fileName: String, competion: @escaping UploadPictureCompletion) {
        
        // upload video file
        storage.child("\(FolderName.MessageVideos)/\(fileName)").putFile(from: fileUrl, metadata: nil) { [weak self] metaData, error in
            guard error == nil else{
                // failed
                print("Failed to upload video file to firebase for message")
                competion(.failure(StorageErrors.failedToUpload))
                return
            }
            
            //download video file and get URL
            self?.storage.child("\(FolderName.MessageVideos)/\(fileName)").downloadURL { url, error in
                guard let url = url else {
                    print("failed To Get Download Url")
                    competion(.failure(StorageErrors.failedToGetDownloadUrl))
                    return
                }
                
                let urlString = url.absoluteString
                print("download url return: \(urlString)")
                
                competion(.success(urlString))
            }
        }
    }
   
    //
    // MARK: download URL from path
    public func downloadURL(for path: String, completion: @escaping (Result<URL, Error>) -> Void) {
        let reference = storage.child(path)
        reference.downloadURL { url, error in
            guard let url = url , error == nil else {
                completion(.failure(StorageErrors.failedToGetDownloadUrl))
                return
            }
            
            completion(.success(url))
        }
    }
}
