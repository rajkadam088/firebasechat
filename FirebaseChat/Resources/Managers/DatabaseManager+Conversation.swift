//
//  DatabaseManager+Conversation.swift
//  FirebaseChat
//
//  Created by Raj Kadam on 22/05/21.
//

import Foundation
import MessageKit
import CoreLocation

// MARK: - Sending messages / conversation to one-one channel
extension DatabaseManager {
   /// Check if conversaion is Exist
    public func conversationExistsWith(targetReciepentEmail: String, completion: @escaping (Result<String, Error>) -> Void) {
        let safeReciepentEmail = DatabaseManager.safeEmail(email: targetReciepentEmail)
        guard let email = SharedPref.shared.getemail() else {
            completion(.failure(DataBaseError.failedToFetch))
            return
        }
        let safeSenderEmail = DatabaseManager.safeEmail(email: email)
        
        database.child("\(FolderName.Users)/\(safeReciepentEmail)/\(ConversationKeys.Conversation)").observeSingleEvent(of: .value) { snapshot in
            
            guard let collection = snapshot.value as? [[String: Any]] else {
                completion(.failure(DataBaseError.failedToFetch))
                return
            }
            // iterate and find conversation with target sender
            if let conversation = collection.first(where: {
                guard let targetSenderEmail = $0[ConversationKeys.OtherUserEmail] as? String, let channelType = $0[ChannelsKey.ChannelType] as? String else{
                    return false
                }
                return safeSenderEmail == targetSenderEmail && channelType.lowercased() == "single"
            }){
               // get Id
                guard let id = conversation["id"] as? String else {
                    completion(.failure(DataBaseError.failedToFetch))
                    return
                }
                completion(.success(id))
                return
                
            }else{
                completion(.failure(DataBaseError.failedToFetch))
                return
            }
        }
    }
    
    /// Create a new conversation with otherUserEmail and message
    public func createNewConversation(with otherUserEmail: String, name: String, firstMessage: Message, completion: @escaping (Bool) -> Void ) {
        guard let currentEmail = SharedPref.shared.getemail() else {
            completion(false)
            return
        }
        let safeEmail = DatabaseManager.safeEmail(email: currentEmail)
        let reference = database.child("\(FolderName.Users)/\(safeEmail)")
        
        reference.observeSingleEvent(of: .value) { [weak self] snapshot in
            guard var userNode = snapshot.value as? [String: Any] else{
                completion(false)
                print("User not found")
                return
            }
            
            let messageDate = firstMessage.sentDate
            let dateString = ChatViewController.dateFormatter.string(from: messageDate)
            
            var message = ""
            switch firstMessage.kind {
            
            case .text(let messageText):
                message = messageText
                break
            case .attributedText(_):
                break
            case .photo(let mediaItem):
                message = mediaItem.url?.absoluteString ?? ""
                break
            case .video(let mediaItem):
                message = mediaItem.url?.absoluteString ?? ""
                break
            case .location( let locationData):
                let location = locationData.location
                message = "\(location.coordinate.longitude),\(location.coordinate.latitude)"
                break
            case .emoji(_):
                break
            case .audio(_):
                break
            case .contact(_):
                break
            case .linkPreview(_):
                break
            case .custom(_):
                break
            }
            let conversationID = "conversation_\(firstMessage.messageId)"
            let senderName = SharedPref.shared.getFullName() ?? "Self"
           
            let newConvesationData: [String: Any] = [
                "id": conversationID,
                ConversationKeys.OtherUserEmail: otherUserEmail,
                ConversationKeys.OtherUserFullName: name,
                ConversationKeys.SenderFullName: senderName,
                ConversationKeys.LatestMessage: [
                    ConversationKeys.messageDate: dateString,
                    ConversationKeys.messageIsRead: false,
                    ConversationKeys.message: message,
                    ConversationKeys.messageType: firstMessage.kind.messageKindString
                ],
                ChannelsKey.ChannelType: "single"
            ]
            
            let reciepent_newConvesationData: [String: Any] = [
                "id": conversationID,
                ConversationKeys.OtherUserEmail: safeEmail,
                ConversationKeys.OtherUserFullName: senderName,
                ConversationKeys.SenderFullName: senderName,
                ConversationKeys.LatestMessage: [
                    ConversationKeys.messageDate: dateString,
                    ConversationKeys.messageIsRead: false,
                    ConversationKeys.message: message,
                    ConversationKeys.messageType: firstMessage.kind.messageKindString
                ],
                ChannelsKey.ChannelType: "single"
            ]
            
            //  Update reciepent User conversation entry
            //let reciept_reference = self?.database.child("\(otherUserEmail)/conversations")
            self?.database.child("\(FolderName.Users)/\(otherUserEmail)/\(ConversationKeys.Conversation)").observeSingleEvent(of: .value) { [weak self] snapshot in
                
                if var conversations =
                    snapshot.value as? [[String: Any]] {
                    // append
                    conversations.append(reciepent_newConvesationData)
                    self?.database.child("\(FolderName.Users)/\(otherUserEmail)/\(ConversationKeys.Conversation)").setValue(conversations)
                }else {
                    // create new
                    self?.database.child("\(FolderName.Users)/\(otherUserEmail)/\(ConversationKeys.Conversation)").setValue([reciepent_newConvesationData])
                }
            }
            // Update current User conversation entry
            if var conversations = userNode[ConversationKeys.Conversation] as? [[String: Any]] {
                // conversation array exists for current user
                // you should append
                conversations.append(newConvesationData)
                userNode[ConversationKeys.Conversation] = conversations
                
                reference.setValue(userNode) { [weak self] error, dateBaseRef in
                    
                    guard error == nil else {
                         completion(false)
                        return
                    }
                    self?.finishCreatingConversation(name: name, conversationId: conversationID, firstMessage: firstMessage, isGroup: false, completion: completion)
                    //completion(true)
                }
            }else {
                // conversation array does not exists for current user
                // you should create it
                userNode[ConversationKeys.Conversation] = [newConvesationData]
                
                reference.setValue(userNode) { [weak self] error, dateBaseRef in
                    
                    guard error == nil else {
                         completion(false)
                        return
                    }
                    self?.finishCreatingConversation(name: name, conversationId: conversationID, firstMessage: firstMessage, isGroup: false, completion: completion)
                    //completion(true)
                }
            }
        }
    }
    
    private func finishCreatingConversation(name: String, conversationId: String, firstMessage: Message, isGroup: Bool, completion: @escaping (Bool) -> Void) {
        guard let currentUserEmail = SharedPref.shared.getemail() else {
            
            completion(false)
            return
        }
        let safeEmail = DatabaseManager.safeEmail(email: currentUserEmail)
        
        var content = ""
        switch firstMessage.kind {
        
        case .text(let messageText):
            content = messageText
            break
        case .attributedText(_):
            break
        case .photo(let mediaItem):
            content = mediaItem.url?.absoluteString ?? ""
            break
        case .video(let mediaItem):
            content = mediaItem.url?.absoluteString ?? ""
            break
        case .location( let locationData):
            let location = locationData.location
            content = "\(location.coordinate.longitude),\(location.coordinate.latitude)"
            break
        case .emoji(_):
            break
        case .audio(_):
            break
        case .contact(_):
            break
        case .linkPreview(_):
            break
        case .custom(_):
            break
        }
        
        let messageDate = firstMessage.sentDate
        let dateString = ChatViewController.dateFormatter.string(from: messageDate)
        let senderName = SharedPref.shared.getFullName() ?? "Sender"
        let message: [String:Any] = ["id":firstMessage.messageId, ConversationKeys.messageType: firstMessage.kind.messageKindString, ConversationKeys.messageContent: content, ConversationKeys.messageDate: dateString, ConversationKeys.SenderEmail: safeEmail, ConversationKeys.messageIsRead: false, ConversationKeys.OtherUserFullName: name, ConversationKeys.SenderFullName: senderName]
        
        let value: [String: Any] = ["isGroup": isGroup
        ]
        /*["messages": [
         message
 ],"isGroup": isGroup
        ]
        */
        let reference = database.child("\(FolderName.ChannelFolderName)/\(conversationId)/messages")
        
        let key = reference.childByAutoId().key ?? "\(firstMessage.messageId)"
        
        database.child("\(FolderName.ChannelFolderName)/\(conversationId)").setValue(value) { error, _ in
            guard error == nil else {
                
                completion(false)
                return
            }
            reference.child("\(key)").updateChildValues(message) { error, _ in
                guard error == nil else {
                    
                    completion(false)
                    return
                }
                completion(true)
            }
            
        }
    }
    
    /// Fetches and returns all conversations for the user with passed in email
    public func getAllConversation(for email: String, completion: @escaping (Result<[Conversation], Error>) -> Void ) {
        
        database.child("\(FolderName.Users)/\(email)/\(ConversationKeys.Conversation)").observe(.value) { snapshot in
            guard let value = snapshot.value as? [[String: Any]] else {
                
                completion(.failure(DataBaseError.failedToFetch))
                return
            }
            var conversations: [Conversation] = value.compactMap ({ dictionary in
                guard let conversationID = dictionary["id"] as? String,
                      let name = dictionary[ConversationKeys.OtherUserFullName] as? String,
                      let otherUserEmail = dictionary[ConversationKeys.OtherUserEmail] as? String,
                      let latestMessage = dictionary[ConversationKeys.LatestMessage] as? [String: Any],
                      let isRead = latestMessage[ConversationKeys.messageIsRead] as? Bool,
                      let sentDate = latestMessage[ConversationKeys.messageDate] as? String,
                      let message = latestMessage[ConversationKeys.message] as? String,
                        let type = latestMessage[ConversationKeys.messageType] as? String,
                        let channelType = dictionary[ChannelsKey.ChannelType] as? String, let senderName = dictionary[ConversationKeys.SenderFullName] as? String
                else {
                    return nil
                }
                
                var groupMembers: [GroupMembers]?
                if let members = dictionary[ChannelsKey.ChannelMembers] as? [[String: Any]] {
                    for member in members {
                        let name = member["name"] as! String
                        let email = member["email"] as! String
                        let is_admin = member["isAdmin"] as! Bool
                        groupMembers?.append(GroupMembers.init(name: name, email: email, isAdmin: is_admin))
                    }
                }
                print("Original Date str: \(sentDate)")
                let message_date = ChatViewController.dateFormatter.date(from: sentDate) ?? Date()
                print("Original formated Date : \(message_date)")
                let msgDateStr = ChatViewController.dateFormatter.string(from: message_date)
                print("new formated Date str: \(msgDateStr)")
                let dateFormatter = ISO8601DateFormatter()
                dateFormatter.timeZone = TimeZone.current
                let new_date = dateFormatter.date(from: msgDateStr) ?? Date()
                print("local Date : \(new_date)")
                let latestMessageObject = LatestMessage.init(messageDate: new_date, date: sentDate, message: message, is_read: isRead, type: type)
                return Conversation.init(id: conversationID, name: name, sender_name: senderName, other_user_email: otherUserEmail, latest_message: latestMessageObject, channel_type: channelType, members: groupMembers)
            })
            
            let calendar = Calendar.current
            conversations.sort {
                let elapsed0 = $0.latest_message.messageDate.timeIntervalSince(calendar.startOfDay(for: $0.latest_message.messageDate))
                let elapsed1 = $1.latest_message.messageDate.timeIntervalSince(calendar.startOfDay(for: $1.latest_message.messageDate))
                return elapsed0 < elapsed1
            }
            
           conversations =  conversations.sorted (by: { $0.latest_message.messageDate > $1.latest_message.messageDate })
            
            completion(.success(conversations))
        }
    }
    
    public func deleteConversation(conversationId: String, completion: @escaping (Bool) -> Void) {
        guard let email = SharedPref.shared.getemail() else {
            completion(false)
            return
        }
        let safeEmail = DatabaseManager.safeEmail(email: email)
        // Get all conversation for current user
        // delete conversation in collection with target id
        //reset those conversations for user and database
        
        let reference = database.child("\(FolderName.Users)/\(safeEmail)/\(ConversationKeys.Conversation)")
        reference.observeSingleEvent(of: .value) { snapshot in
            if  var conversations = snapshot.value as? [[String: Any]] {
                var positionToRemove = 0
                for conversation in conversations {
                    if let id = conversation["id"] as? String, id == conversationId {
                        print("Found conversation to delete")
                        break
                    }
                    positionToRemove += 1
                    
                }
                conversations.remove(at: positionToRemove)
                
                 // update conversation array
                reference.setValue(conversations) { error, _ in
                    guard error == nil else {
                        print("Failed to write new conversation")
                        completion(false)
                        return
                    }
                    print("Deleted conversation")
                    completion(true)
                    
                }
            }
        }
        
    }
}
