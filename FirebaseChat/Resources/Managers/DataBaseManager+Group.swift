//
//  DataBaseManager+Group.swift
//  FirebaseChat
//
//  Created by Raj Kadam on 31/05/21.
//

import Foundation

import MessageKit
import CoreLocation

// MARK: - Creating  group channel
extension DatabaseManager {
    
    func createGroupID(name: String, email: String) -> String? {
        // date, otherUserEmail, SenderEmail, randomInt
        let dateString = ChatViewController.dateFormatter.string(from: Date())
        
        let newIdentifier = "group_\(name)_\(email)_\(dateString)"
        
        print("Created group id: \(newIdentifier)")
        return newIdentifier
    }
    
    /// Create a new group conversation with otherUserEmail and message
    public func createNewGroupConversation(with memberList: [SearchResult], groupName: String, firstMessage: Message, completion: @escaping (Bool) -> Void ) {
        guard let currentEmail = SharedPref.shared.getemail() else {
            completion(false)
            return
        }
        let safeEmail = DatabaseManager.safeEmail(email: currentEmail)
        guard let conversationID = createGroupID(name: groupName, email: safeEmail) else {
            completion(false)
            return
        }
        let reference = database.child("\(FolderName.Users)/\(safeEmail)")
        reference.observeSingleEvent(of: .value) { [weak self] snapshot in
            guard var userNode = snapshot.value as? [String: Any] else{
                completion(false)
                print("User not found")
                return
            }
            
            let messageDate = firstMessage.sentDate
            let dateString = ChatViewController.dateFormatter.string(from: messageDate)
            
            var message = ""
            switch firstMessage.kind {
            
            case .text(let messageText):
                message = messageText
                break
            case .attributedText(_):
                break
            case .photo(let mediaItem):
                message = mediaItem.url?.absoluteString ?? ""
                break
            case .video(let mediaItem):
                message = mediaItem.url?.absoluteString ?? ""
                break
            case .location( let locationData):
                let location = locationData.location
                message = "\(location.coordinate.longitude),\(location.coordinate.latitude)"
                break
            case .emoji(_):
                break
            case .audio(_):
                break
            case .contact(_):
                break
            case .linkPreview(_):
                break
            case .custom(_):
                break
            }
            let senderName = SharedPref.shared.getFullName() ?? "Self"
            var memberEmails = ""
            let adminUser: [String: Any] = ["name": senderName, "email": safeEmail, "isAdmin": true]
            let messageSender: [String: Any] = ["name": senderName, "email": safeEmail]
            var groupMembers: [[String: Any]] = [[String: Any]]()
            for member in memberList {
                memberEmails = "\(memberEmails),\(member.email)"
                groupMembers.append(["name": member.name, "email": member.email, "isAdmin": false])
            }
            groupMembers.append(adminUser)
            if memberEmails.hasPrefix(",") {
                memberEmails = String(memberEmails.dropFirst(1))
            }
            
            
            let newConvesationData: [String: Any] = [
                "id": conversationID,
                ConversationKeys.OtherUserEmail: "",
                ConversationKeys.OtherUserFullName: groupName,
                ConversationKeys.SenderFullName: senderName,
                ConversationKeys.LatestMessage: [
                    ConversationKeys.messageDate: dateString,
                    ConversationKeys.messageIsRead: false,
                    ConversationKeys.message: message,
                    ConversationKeys.messageType: firstMessage.kind.messageKindString
                ],
                ChannelsKey.ChannelType: "group"
                
                
            ]
            let reciepent_newConvesationData: [String: Any] = [
                "id": conversationID,
                ConversationKeys.OtherUserEmail: safeEmail,
                ConversationKeys.OtherUserFullName: groupName,
                ConversationKeys.SenderFullName: senderName,
                ConversationKeys.LatestMessage: [
                    ConversationKeys.messageDate: dateString,
                    ConversationKeys.messageIsRead: false,
                    ConversationKeys.message: message,
                    ConversationKeys.messageType: firstMessage.kind.messageKindString
                ],
                ChannelsKey.ChannelType: "group"
            ]
            
            //  Update all reciepent Users conversation entry
            //let reciept_reference = self?.database.child("\(otherUserEmail)/conversations")
            for member in memberList {
                let otherUserEmail = member.email
            self?.database.child("\(FolderName.Users)/\(otherUserEmail)/\(ConversationKeys.Conversation)").observeSingleEvent(of: .value) { [weak self] snapshot in
                
                if var conversations =
                    snapshot.value as? [[String: Any]] {
                    // append
                    conversations.append(reciepent_newConvesationData)
                    self?.database.child("\(FolderName.Users)/\(otherUserEmail)/\(ConversationKeys.Conversation)").setValue(conversations)
                }else {
                    // create new
                    self?.database.child("\(FolderName.Users)/\(otherUserEmail)/\(ConversationKeys.Conversation)").setValue([reciepent_newConvesationData])
                }
            }
            
        }
            // Update current User conversation entry
            if var conversations = userNode[ConversationKeys.Conversation] as? [[String: Any]] {
                // conversation array exists for current user
                // you should append
                conversations.append(newConvesationData)
                userNode[ConversationKeys.Conversation] = conversations
                reference.setValue(userNode) { [weak self] error, dateBaseRef in
                    
                    guard error == nil else {
                         completion(false)
                        return
                    }
                    self?.finishCreatingGroupConversation(name: groupName, conversationId: conversationID, firstMessage: firstMessage, members: groupMembers, messageSender: messageSender, completion: completion)
                    //completion(true)
                }
            }else {
                // conversation array does not exists for current user
                // you should create it
                userNode[ConversationKeys.Conversation] = [newConvesationData]
                
                reference.setValue(userNode) { [weak self] error, dateBaseRef in
                    
                    guard error == nil else {
                         completion(false)
                        return
                    }
                    self?.finishCreatingGroupConversation(name: groupName, conversationId: conversationID, firstMessage: firstMessage, members: groupMembers, messageSender: messageSender, completion: completion)
                    //completion(true)
                }
            }
        }
    }
    
    private func finishCreatingGroupConversation(name: String, conversationId: String, firstMessage: Message, members: [[String: Any]], messageSender: [String: Any], completion: @escaping (Bool) -> Void) {
        guard let currentUserEmail = SharedPref.shared.getemail() else {
            
            completion(false)
            return
        }
        let safeEmail = DatabaseManager.safeEmail(email: currentUserEmail)
        
        var content = ""
        switch firstMessage.kind {
        
        case .text(let messageText):
            content = messageText
            break
        case .attributedText(_):
            break
        case .photo(let mediaItem):
            content = mediaItem.url?.absoluteString ?? ""
            break
        case .video(let mediaItem):
            content = mediaItem.url?.absoluteString ?? ""
            break
        case .location( let locationData):
            let location = locationData.location
            content = "\(location.coordinate.longitude),\(location.coordinate.latitude)"
            break
        case .emoji(_):
            break
        case .audio(_):
            break
        case .contact(_):
            break
        case .linkPreview(_):
            break
        case .custom(_):
            break
        }
        
        let messageDate = firstMessage.sentDate
        let dateString = ChatViewController.dateFormatter.string(from: messageDate)
        let senderName = SharedPref.shared.getFullName() ?? "Sender"
        let message: [String:Any] = ["id":firstMessage.messageId, ConversationKeys.messageType: firstMessage.kind.messageKindString, ConversationKeys.messageContent: content, ConversationKeys.messageDate: dateString, ConversationKeys.SenderEmail: safeEmail, ConversationKeys.messageIsRead: false, ConversationKeys.OtherUserFullName: name, ConversationKeys.SenderFullName: senderName, ConversationKeys.MessageReadUsers: [messageSender]]
        
        let value: [String: Any] = ["isGroup": true,
        ChannelsKey.ChannelMembers: members
        ]
        /*
         ["messages": [
                     message
         ], "isGroup": true,
         ChannelsKey.ChannelMembers: members
         ]
         */
        let reference = database.child("\(FolderName.ChannelFolderName)/\(conversationId)/messages")
        
        let key = reference.childByAutoId().key ?? "\(firstMessage.messageId)"
        
        database.child("\(FolderName.ChannelFolderName)/\(conversationId)").setValue(value) { error, _ in
            guard error == nil else {
                
                completion(false)
                return
            }
            reference.child("\(key)").updateChildValues(message) { error, _ in
                guard error == nil else {
                    
                    completion(false)
                    return
                }
                completion(true)
            }
            //completion(true)
        }
    }
    
}

// MARK: - Fetch Group Members

extension DatabaseManager {
    
    public func getMembersFor(group conversationId: String, completion: @escaping(Result<[GroupMembers], Error>) -> Void ) {
        
        database.child("\(FolderName.ChannelFolderName)/\(conversationId)/\(ChannelsKey.ChannelMembers)").observeSingleEvent(of: .value) { snapshot in
            guard let value = snapshot.value as? [[String: Any]] else {
                
                completion(.failure(DataBaseError.failedToFetch))
                return
            }
            
            let members: [GroupMembers] = value.compactMap { member in
                
                guard let name = member["name"] as? String,
                      let email = member["email"] as? String,
                      let isAdmin = member["isAdmin"] as? Bool else {
                    return nil
                }
                
                return GroupMembers.init(name: name, email: email, isAdmin: isAdmin)
            }
            
            completion(.success(members))
            
        }
        
    }
}

// MARK: Group members
extension DatabaseManager {
    /// get group members list
    func getGroupMembers(groupId: String, completion: @escaping(Result<[GroupMembers], Error>) -> Void) {
        
        database.child("\(FolderName.ChannelFolderName)/\(groupId)/members").observeSingleEvent(of: .value) { snapshot in
            guard let members = snapshot.value as? [[String: Any]] else {
                print("failed to fetch group members")
                completion(.failure(DataBaseError.failedToFetch))
                return
            }
            var groupMembers: [GroupMembers] = [GroupMembers]()
            for member in members {
                let name = member["name"] as! String
                let email = member["email"] as! String
                let is_admin = member["isAdmin"] as! Bool
                groupMembers.append(GroupMembers.init(name: name, email: email, isAdmin: is_admin))
            }
            if members.count == 0, groupMembers == nil {
                print("failed to fetch group members")
                completion(.failure(DataBaseError.failedToFetch))
            }else{
                completion(.success(groupMembers))
            }
        }
            
    }
}
