//
//  DatabaseManager+Messages.swift
//  FirebaseChat
//
//  Created by Raj Kadam on 14/06/21.
//

import Foundation
import MessageKit
import CoreLocation
extension DatabaseManager {
    /// get all messages for all conversation
    public func getAllMessagesForConversation(with id: String, completion: @escaping (Result<[Message], Error>) -> Void ) {
        
        
        
        database.child("\(FolderName.ChannelFolderName)/\(id)/messages").observe(.value) { [weak self] snapshot in
            guard let value = snapshot.value as? [String: [String: Any]], let strongSelf = self else {
                
                completion(.failure(DataBaseError.failedToFetch))
                return
            }
            print("VAlues",value)
            let ar = value.map { $0.value }
            print("Ar",ar)
            var messages: [Message] = ar.compactMap ({ dictionary in
                guard let name = dictionary[ConversationKeys.OtherUserFullName] as? String,
                      let isRead = dictionary[ConversationKeys.messageIsRead] as? Bool,
                      let type = dictionary[ConversationKeys.messageType] as? String,
                      let messageId = dictionary["id"] as? String,
                      let senderEmail = dictionary[ConversationKeys.SenderEmail] as? String,
                      let dateString = dictionary[ConversationKeys.messageDate] as? String,
                      let content = dictionary[ConversationKeys.messageContent] as? String,
                      let senderName = dictionary[ConversationKeys.SenderFullName] as? String else {
                    return nil
                }
                
                let date = ChatViewController.dateFormatter.date(from: dateString) ?? Date()
                let sender = Sender.init(photoURL: "", senderId: senderEmail, displayName: name)
                return Message.init(sender: sender, messageId: messageId, sentDate: date, kind: strongSelf.getMessageType(type: type, content: content), senderName: senderName, senderEmail: senderEmail, isRead: isRead)
            })
            
            let calendar = Calendar.current
            messages.sort {
                let elapsed0 = $0.sentDate.timeIntervalSince(calendar.startOfDay(for: $0.sentDate))
                let elapsed1 = $1.sentDate.timeIntervalSince(calendar.startOfDay(for: $1.sentDate))
                return elapsed0 < elapsed1
            }
            
            completion(.success(messages))
        }
    }
    
    private func getMessageType(type: String, content: String) -> MessageKind {
        
         if type == "photo" {
            return MessageKind.photo(Media.init(url: URL.init(string: content), image: nil, placeholderImage: UIImage.init(systemName: "plus")!, size: CGSize.init(width: 250, height: 150)))
        }else if type == "video" {
            return MessageKind.video(Media.init(url: URL.init(string: content), image: nil, placeholderImage: UIImage.init(systemName: "play.rectangle.fill")!, size: CGSize.init(width: 250, height: 150)))
        }else if type == "location" {
            let locationArray = content.components(separatedBy: ",")
            guard locationArray.count == 2, let lat = Double(locationArray[1]), let long = Double(locationArray[0]) else {
                return MessageKind.text(content)
            }
            return MessageKind.location(Location.init(location: CLLocation.init(latitude: lat, longitude: long), size: CGSize.init(width: 250, height: 150)))
        }
        else {
            // text
            return MessageKind.text(content)
        }
       
    }
    
    /// send a message with target conversation
    public func sendMessageTo(convesationId: String, otherUserEmail: String?, name: String, newMessage : Message, groupMembers: [GroupMembers]? = nil, completion: @escaping (Bool) -> Void ) {
        
        // update sender latest message
        // update receipent latest message
        
        guard let email = SharedPref.shared.getemail() else {
            completion(false)
            return
        }
        let currentUserEmail = DatabaseManager.safeEmail(email: email)
        
        // first check channeltype
        var channelType = "single"
        if groupMembers != nil {
            channelType = "group"
        }
        
        let newDatabaseRef = database.child("\(FolderName.ChannelFolderName)/\(convesationId)/messages")//.childByAutoId()
        let key = newDatabaseRef.childByAutoId().key ?? "\(newMessage.messageId)"
        //newDatabaseRef.updateChildValues(<#T##values: [AnyHashable : Any]##[AnyHashable : Any]#>)
        
//        database.child("\(FolderName.ChannelFolderName)/\(convesationId)/messages")
        newDatabaseRef.observeSingleEvent(of: .value) { [weak self] snapshot in
            guard let strongSelf = self else {
                completion(false)
                return
            }
            // add new message to messages
            guard var currentMessages = snapshot.value as? [String: Any] else {
                completion(false)
                return
            }
            var content = ""
            switch newMessage.kind {
            
            case .text(let messageText):
                content = messageText
                break
            case .attributedText(_):
                break
            case .photo(let mediaItem):
                content = mediaItem.url?.absoluteString ?? ""
                break
            case .video(let mediaItem):
                content = mediaItem.url?.absoluteString ?? ""
                break
            case .location( let locationData):
                let location = locationData.location
                content = "\(location.coordinate.longitude),\(location.coordinate.latitude)"
                break
            case .emoji(_):
                break
            case .audio(_):
                break
            case .contact(_):
                break
            case .linkPreview(_):
                break
            case .custom(_):
                break
            }
            
            let messageDate = newMessage.sentDate
            let senderName = SharedPref.shared.getFullName() ?? "Sender"
               
            let dateString = ChatViewController.dateFormatter.string(from: messageDate)
            
            var newMessageEntry: [String:Any] = [String: Any]()
            newMessageEntry = ["id":newMessage.messageId, ConversationKeys.messageType: newMessage.kind.messageKindString, ConversationKeys.messageContent: content, ConversationKeys.messageDate: dateString, ConversationKeys.SenderEmail: currentUserEmail, ConversationKeys.messageIsRead: false, ConversationKeys.OtherUserFullName: name, ConversationKeys.SenderFullName: senderName]
            
            if channelType == "group" {
                let sender: [String: Any] = ["name": senderName, "email": currentUserEmail]
                newMessageEntry[ConversationKeys.MessageReadUsers] = [sender]
            }
            
            
            //currentMessages.append(newMessageEntry)
            newDatabaseRef.child("\(key)").updateChildValues(newMessageEntry) { error, _ in
                
                guard error == nil else {
                    completion(false)
                    return
                }
                
                // Update current User and Reciepient Conversation latest message
                let updatedValue: [String: Any] = [
                    ConversationKeys.messageDate: dateString,
                    ConversationKeys.messageIsRead: false,
                    ConversationKeys.message: content,
                    ConversationKeys.messageType: newMessage.kind.messageKindString
                ]
                
                
                strongSelf.updateCurrenUserConversationLatestMessage(currentUserEmail: currentUserEmail, otherUserEmail: otherUserEmail, convesationId: convesationId, updatedValue: updatedValue, otherUserName: name, channelType: channelType, groupMembers: groupMembers, completion: completion)
                 
            }
            /*strongSelf.database.child("\(FolderName.ChannelFolderName)/\(convesationId)/messages").setValue(currentMessages) { error, _ in
                
                guard error == nil else {
                    completion(false)
                    return
                }
                
                // Update current User and Reciepient Conversation latest message
                let updatedValue: [String: Any] = [
                    ConversationKeys.messageDate: dateString,
                    ConversationKeys.messageIsRead: false,
                    ConversationKeys.message: content,
                    ConversationKeys.messageType: newMessage.kind.messageKindString
                ]
                
                
                strongSelf.updateCurrenUserConversationLatestMessage(currentUserEmail: currentUserEmail, otherUserEmail: otherUserEmail, convesationId: convesationId, updatedValue: updatedValue, otherUserName: name, channelType: channelType, groupMembers: groupMembers, completion: completion)
                 
            }*/
        }
        
        
    }
    
    private func updateCurrenUserConversationLatestMessage(currentUserEmail: String, otherUserEmail: String?, convesationId: String, updatedValue: [String: Any], otherUserName: String, channelType: String, groupMembers: [GroupMembers]? = nil, completion: @escaping (Bool) -> Void) {
        
        database.child("\(FolderName.Users)/\(currentUserEmail)/\(ConversationKeys.Conversation)").observeSingleEvent(of: .value) { [weak self] snapshot  in
            
            guard let strongSelf = self else {
             completion(false)
                return
            }
            var receiverEmail = ""
            if let email = otherUserEmail {
                receiverEmail = email
            }
            if var currentUserConversation = snapshot.value as? [[String :Any]]  {
                
                // find conversation for passed conversationId
                var position = 0
                var targetConversation: [String: Any]?
                for conversation in currentUserConversation {
                    if let current_id = conversation["id"] as? String, current_id == convesationId {
                        targetConversation = conversation
                        break
                        
                    }
                    position += 1
                }
               
                if var targetConversation = targetConversation {
                targetConversation[ConversationKeys.LatestMessage] = updatedValue
                currentUserConversation[position] = targetConversation
                   
                }else{
                    // Failed to find in current collection
                    // check if it is group message
                    // yes then chnage otherUserEmail to empty
                   
                    let newConvesationData: [String: Any] = [
                        "id": convesationId,
                        ConversationKeys.OtherUserEmail: receiverEmail,
                        ConversationKeys.OtherUserFullName: otherUserName,
                        ConversationKeys.LatestMessage: updatedValue,
                        ChannelsKey.ChannelType: channelType
                    ]
                    
                    currentUserConversation.append(newConvesationData)
                    
                }
                strongSelf.database.child("\(FolderName.Users)/\(currentUserEmail)/\(ConversationKeys.Conversation)").setValue(currentUserConversation) { error, _ in
                    guard error == nil else {
                        completion(false)
                        return
                    }
                    
                    // update latest message for Receipient
                    strongSelf.updateReceipientLatestMessage(otherUserEmail: otherUserEmail, convesationId: convesationId, updatedValue: updatedValue, channelType: channelType, groupMembers: groupMembers, otherUserName: otherUserName, completion: completion)
                }
                
                
            }else{
                // current collection does not exist
                let newConvesationData: [String: Any] = [
                    "id": convesationId,
                    ConversationKeys.OtherUserEmail: receiverEmail,
                    ConversationKeys.OtherUserFullName: otherUserName,
                    ConversationKeys.LatestMessage: updatedValue,
                    ChannelsKey.ChannelType: channelType
                ]
                
                self?.database.child("\(FolderName.Users)/\(currentUserEmail)/\(ConversationKeys.Conversation)").setValue([newConvesationData])
                // update latest message for Receipient
                strongSelf.updateReceipientLatestMessage(otherUserEmail: otherUserEmail, convesationId: convesationId, updatedValue: updatedValue, channelType: channelType, groupMembers: groupMembers, otherUserName: otherUserName, completion: completion)
            }
            
        }
    }
  
    
    private func updateReceipientLatestMessage(otherUserEmail: String?, convesationId: String, updatedValue: [String: Any], channelType: String, groupMembers: [GroupMembers]? = nil,otherUserName: String, completion: @escaping (Bool) -> Void) {
        
        guard let email = SharedPref.shared.getemail() else {
            completion(false)
            return
        }
        let currentUserEmail = DatabaseManager.safeEmail(email: email)
        var receiverEmail = ""
        if let email = otherUserEmail {
            receiverEmail = email
        }
        let reciepent_newConvesationData: [String: Any] = [
            "id": convesationId,
            ConversationKeys.OtherUserEmail: currentUserEmail,
            ConversationKeys.OtherUserFullName: otherUserName,
            ConversationKeys.LatestMessage: updatedValue,
            ChannelsKey.ChannelType: channelType
        ]
        if let memberList = groupMembers {
           var updatedReciepentCount = 0
            var iterations = 0
            for i in 0...memberList.count-1 {
                let member = memberList[i]
                let email = member.email
                updateRecipientMember(convesationId: convesationId, receiverEmail: email, reciepent_newConvesationData: reciepent_newConvesationData, updatedValue: updatedValue) { count in
                    print("Read Count:\n\(count)")
                    iterations += 1
                    //print("i:\(i)\n memberList count: \(memberList.count)")
                    updatedReciepentCount = updatedReciepentCount+count
                    print("i:\(i)\n memberList count: \(memberList.count) \n updatedReciepentCount: \(updatedReciepentCount)")
                    
                    if count == 1 {
                        
                        if updatedReciepentCount == memberList.count, iterations == memberList.count {
                            completion(true)
                        }else if iterations == memberList.count{
                            completion(false)
                        }else{
                        
                        }
                        
                        /*if position == memberList.count-1 {
                         if updatedReciepentCount == memberList.count {
                         completion(true)
                         }else {
                         completion(false)
                         }
                         }*/
                    }else{
                        print("Unable to update receiver: \(email)")
                    }
                }
            }
           
        }else{
            updateRecipientMember(convesationId: convesationId, receiverEmail: receiverEmail, reciepent_newConvesationData: reciepent_newConvesationData, updatedValue: updatedValue) { count  in
                completion(true)
            }
        }
    }
    
    func updateRecipientMember(convesationId: String, receiverEmail: String, reciepent_newConvesationData: [String: Any], updatedValue: [String: Any] , completion: @escaping (Int) -> Void) {
        database.child("\(FolderName.Users)/\(receiverEmail)/\(ConversationKeys.Conversation)").observeSingleEvent(of: .value) { [weak self] snapshot  in
            guard let strongSelf = self else {
             completion(0)
                return
            }
            if var otherUserConversation = snapshot.value as? [[String :Any]]  {
                // find conversation for passed conversationId
                var position = 0
                var targetConversation: [String: Any]?
                for conversation in otherUserConversation {
                    if let current_id = conversation["id"] as? String, current_id == convesationId {
                        targetConversation = conversation
                        break
                        
                    }
                    position += 1
                }
               
                if var targetConversation = targetConversation {
                    targetConversation[ConversationKeys.LatestMessage] = updatedValue
                    otherUserConversation[position] = targetConversation
                    
                }else{
                    // failed to find in cureent collection
                   
                    otherUserConversation.append(reciepent_newConvesationData)
                }
                
                strongSelf.database.child("\(FolderName.Users)/\(receiverEmail)/\(ConversationKeys.Conversation)").setValue(otherUserConversation) { error, _ in
                    guard error == nil else {
                        completion(0)
                        return
                    }
                    
                    completion(1)
                    
                }
            }else{
                // current collection does not exist
                self?.database.child("\(FolderName.Users)/\(receiverEmail)/\(ConversationKeys.Conversation)").setValue([reciepent_newConvesationData])
                completion(1)
            }
        }
    }
}

//MARK: Read Messages
extension DatabaseManager {
    /// read one-one conversation
    func readOneToOneConversation(conversationId: String, completion: @escaping (Bool) -> Void){
        
        guard let currentEmail = SharedPref.shared.getemail() else {
            completion(false)
            return
        }
        
        let currentUserSafeEmail = DatabaseManager.safeEmail(email: currentEmail)
        database.child("\(FolderName.ChannelFolderName)/\(conversationId)/messages").observeSingleEvent(of: .value) { [weak self] snapshot in
            
            guard var messages = snapshot.value as? [[String: Any]], let strongSelf = self else {
                
                completion(false)
                return
            }
            
            for i in 0 ... messages.count-1 {
                var message = messages[i]
                if let msg_sender = message[ConversationKeys.SenderEmail] as? String, let isRead = message[ConversationKeys.messageIsRead] as? Bool, msg_sender != currentUserSafeEmail, !isRead {
                    
                    message[ConversationKeys.messageIsRead] = true
                    messages[i] = message
                }
               
            }
            
            strongSelf.database.child("\(FolderName.ChannelFolderName)/\(conversationId)/messages").setValue(messages) { error, _ in
                guard error == nil else {
                    completion(false)
                    return
                }
                completion(true)
            }
            
        }
    }
    
    
    /// read group conversation
    func readGroupConversation(conversationId: String, completion: @escaping (Bool) -> Void){
        
        guard let currentEmail = SharedPref.shared.getemail() else {
            completion(false)
            return
        }
        
        let currentUserSafeEmail = DatabaseManager.safeEmail(email: currentEmail)
        let senderName = SharedPref.shared.getFullName() ?? "Self"
        let currentUser: [String: Any] = ["name": senderName, "email": currentUserSafeEmail]
        
        self.getGroupMembers(groupId: conversationId) { [weak self] result in
            
            guard let strongSelf = self else {
                completion(false)
                return
                
            }
            var groupMembers = [GroupMembers]()
            switch result {
            
            case .success(let members):
                groupMembers = members
            case .failure(_):
                break
            }

            strongSelf.database.child("\(FolderName.ChannelFolderName)/\(conversationId)/messages").observeSingleEvent(of: .value) { snapshot in
                
                guard var messages = snapshot.value as? [[String: Any]] else {
                    completion(false)
                    return
                }
                
                for i in 0 ... messages.count-1 {
                    var message = messages[i]
                    if let msg_sender = message[ConversationKeys.SenderEmail] as? String, let isRead = message[ConversationKeys.messageIsRead] as? Bool, msg_sender != currentUserSafeEmail, !isRead, var readReciepents = message[ConversationKeys.MessageReadUsers] as? [[String: Any]] {
                        
                        if let _ = readReciepents.first(where: {
                            guard let userEmail = $0["email"] as? String else{
                                return false
                            }
                            return currentUserSafeEmail == userEmail
                        }){
                          // user alerady read message
                        }else{
                            /// add current user o read array
                            readReciepents.append(currentUser)
                            message[ConversationKeys.MessageReadUsers] = readReciepents
                            /// check if all users read given message
                            if groupMembers.count == readReciepents.count {
                                /// mark all read
                                message[ConversationKeys.messageIsRead] = true
                            }else{
                                /// do nothing
                            }
                        }
                        
                        messages[i] = message
                    }
                }
                
                strongSelf.database.child("\(FolderName.ChannelFolderName)/\(conversationId)/messages").setValue(messages) { error, _ in
                    guard error == nil else {
                        completion(false)
                        return
                    }
                    completion(true)
                }
                
            }
        }
    }
}
