//
//  SharedPref.swift
//  FirebaseChat
//
//  Created by Raj Kadam on 22/05/21.
//

import Foundation
import UIKit

final class SharedPref {
    
    static let shared = SharedPref()
    
    
    func deleteAll(){
        setemail(email: "")
        setProfileImage(url: nil)
        setlastName(name: "")
        setfirstName(name: "")
    }
    func setfirstName(name: String){
        UserDefaults.standard.set(name, forKey: "first_name")
    }
    
    func setlastName(name: String){
        UserDefaults.standard.set(name, forKey: "last_name")
    }
    
    func getFullName() -> String?{
        return (UserDefaults.standard.string(forKey: "first_name") ?? "") + " " + (UserDefaults.standard.string(forKey: "last_name") ?? "")
    }
    
    func setemail(email: String){
        UserDefaults.standard.set(email, forKey: "email")
    }
    
    
    func getemail() -> String?{
        return UserDefaults.standard.string(forKey: "email")
    }
    
    func setProfileImage(url: URL?){
        UserDefaults.standard.set(url, forKey: "profile_image")
    }
    func getProfileImage() -> URL? {
        return UserDefaults.standard.url(forKey: "profile_image")
    }
    
    
   
}

