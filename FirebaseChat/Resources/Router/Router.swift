//
//  Router.swift
//  FirebaseChat
//
//  Created by Raj Kadam on 22/05/21.
//

import Foundation
import UIKit

struct Router
{
    
    static func gotoConversation(){
        
        let storyboard: UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let tabBar_VC: UITabBarController = storyboard.instantiateViewController(withIdentifier: "TabBar") as! UITabBarController
        tabBar_VC.selectedIndex = 0
        //let nav = UINavigationController(rootViewController: tabBar_VC)
        //nav.navigationBar.isHidden = true
        //nav.isNavigationBarHidden = true
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.window?.rootViewController = nav
        
        UIApplication.shared.windows.first?.rootViewController = tabBar_VC
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        
    }
    static func gotoLogin(){
        
        let storyboard: UIStoryboard = UIStoryboard.init(name: "Register", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        
        let nav = UINavigationController(rootViewController: vc)
        UIApplication.shared.windows.first?.rootViewController = nav
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        
        
    }
    
}
