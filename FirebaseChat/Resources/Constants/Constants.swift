//
//  Constants.swift
//  FirebaseChat
//
//  Created by Raj Kadam on 22/05/21.
//

import Foundation

struct UserConstants {
    
    static let profilePicFileNameExtension = "_profile_picture.png"
}

struct FolderName {
    
    static let MessageImages = "message_images"
    static let MessageVideos = "message_videos"
    static let ProfileImage = "images"
    static let Users = "Users"
    static let SearchChannels = "Search_Channels"
    static let ChannelFolderName = "Channels"
}
struct UserDatakey {
    
    static let firstName = "firstName"
    static let lastName = "lastName"
    static let name = "name"
    static let email = "email"
    
}

struct ChannelsKey {
   
    static let ChannelType = "channel_type"
    static let ChannelMembers = "members"
}

struct ConversationKeys {
    static let Conversation = "conversations"
    static let OtherUserEmail = "other_user_email"
    static let OtherUserFullName = "name"
    static let SenderFullName = "sender_name"
    static let LatestMessage = "latest_message"
    static let messageDate = "date"
    static let message = "message"
    static let messageIsRead = "is_read"
    static let MessageReadUsers = "read_list"
    static let messageContent = "content"
    static let messageType = "type"
    static let SenderEmail = "sender_email"
    
}
