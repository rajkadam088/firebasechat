//
//  ProfileViewModel.swift
//  FirebaseChat
//
//  Created by Raj Kadam on 26/05/21.
//

import Foundation

struct ProfileViewModel {
    let viewModelType: ProfileViewModelType
    let title: String
    let handler: (() -> Void)?
}

enum ProfileViewModelType {
    case info, logout
}
